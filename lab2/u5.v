module u5(clk_50mhz, sw, GPIO, led7a, led7b, led7d, led7e, led7f, led7g, led7h, ledr);
//asdfasdf
input [14:0] sw;
input clk_50mhz;
output reg[1:0] GPIO;
output reg [3:0] ledr;
reg [20:0] f11;
reg [20:0] f21;
reg [20:0] F1;
reg [20:0] F2;
reg [6:0] C1;
reg [6:0] C2;
reg [25:0] count;
reg sync1 = 0, sync2 = 0;
output reg[6:0] led7a;
output reg[6:0] led7b;
output reg[6:0] led7d;
output reg[6:0] led7e;
output reg[6:0] led7f;
output reg[6:0] led7g;
output reg[6:0] led7h;
initial 
begin
ledr = 4'b0000;
end
always @ (sw)

begin
	case(sw[2:0])
		0:	F1 <= 1;
		1:	F1 <= 2;
		2:	F1 <= 4;
		3:	F1 <= 8;
		4:	F1 <= 16;
		5:	F1 <= 32;
		6:	F1 <= 64;
		7:	F1 <= 128;
	endcase
	case(sw[10:8])
		0:	F2 <= 100;
		1:	F2 <= 200;
		2:	F2 <= 400;
		3:	F2 <= 800;
		4:	F2 <= 1600;
		5:	F2 <= 3200;
		6:	F2 <= 6400;
		7:	F2 <= 12800;
	endcase
	case(sw[6:4])
		0:	C1 <= 10;
		1:	C1 <= 20;
		2:	C1 <= 30;
		3:	C1 <= 40;
		4:	C1 <= 50;
		5:	C1 <= 60;
		6:	C1 <= 70;
		7:	C1 <= 80;
	endcase
	case(sw[14:12])
		0:	C2 <= 10;
		1:	C2 <= 20;
		2:	C2 <= 30;
		3:	C2 <= 40;
		4:	C2 <= 50;
		5:	C2 <= 60;
		6:	C2 <= 70;
		7:	C2 <= 80;
	endcase
	f11 = (50000000 / F1) * C1 / 100;
	f21 = (50000000 / F2) * C2 / 100;
	/*GPIO[0] = 1'b1;
	GPIO[1] = 1'b1;
	count = 0;*/
	sync1 = !sync1;
	printer();//printout
end

always @ (posedge clk_50mhz)
begin
	if (sync1 != sync2) 
		begin
			GPIO[0] = 1'b1;
			GPIO[1] = 1'b1;
			count = 0;
			sync2 <= !sync2;
			ledr[2] = sync2;
		end
	else;
	if((count - f11) % (50000000 / F1) == 0)
		begin
		GPIO[0] = 1'b0;
		ledr[0] = 1'b0;
		end
	else;
	if((count - f21) % (50000000 / F2) == 0)
		GPIO[1] = 1'b0;
	else;
	if(count % (50000000 / F1) == 0)
		begin
		GPIO[0] = 1'b1;
		ledr[0] = 1'b1;
		end
	else;
	if(count % (50000000 / F2) == 0)
		GPIO[1] = 1'b1;
	else;
	if(count >= 49999999)
		begin
			count = 0;
		end
	else;
	count = count + 1;
end
	task printer ;
    begin
        case(C1/10)     
            1:  led7a<=8'b11111001;//1
            2:  led7a<=8'b10100100;//2
            3:  led7a<=8'b10110000;//3
            4:  led7a<=8'b10011001;//4     
            5:  led7a<=8'b10010010;//5
            6:  led7a<=8'b10000010;//6
            7:  led7a<=8'b11111000;//7
            8:  led7a<=8'b10000000;//8     
            9:  led7a<=8'b10011000;//9
            default :led7a<=8'b10000000;//8
        endcase
        led7b<=8'b11000000;//0
		case(F1/10000)
			1:  led7d<=8'b11111001;//1
            default :led7d<=8'b11111111;//nothing
		endcase
		case((F1%10000)/1000)
			1:  led7e<=8'b11111001;//1
            2:  led7e<=8'b10100100;//2
            3:  led7e<=8'b10110000;//3
            4:  led7e<=8'b10011001;//4     
            5:  led7e<=8'b10010010;//5
            6:  led7e<=8'b10000010;//6
            7:  led7e<=8'b11111000;//7
            8:  led7e<=8'b10000000;//8     
            9:  led7e<=8'b10011000;//9
            default :led7e<=8'b11111111;//nothing
        endcase
		case((F1%1000)/100)
			1:  led7f<=8'b11111001;//1
            2:  led7f<=8'b10100100;//2
            3:  led7f<=8'b10110000;//3
            4:  led7f<=8'b10011001;//4     
            5:  led7f<=8'b10010010;//5
            6:  led7f<=8'b10000010;//6
            7:  led7f<=8'b11111000;//7
            8:  led7f<=8'b10000000;//8     
            9:  led7f<=8'b10011000;//9
            default :led7f<=8'b11111111;//nothing
        endcase
		led7h<=8'b11000000;//0
		led7g<=8'b11000000;//0
	end
    endtask

endmodule