module u1(clk_50mhz, sw, led70o,led7o);
		input clk_50mhz;
        input [1:2]sw;
        output [7:0]led70o;
        output [7:0]led7o;
        reg [7:0]led70r;
        reg [7:0]led7r;
        reg r;// to  remenber the state of counting way
        reg [25:0] count;
        reg a=0;
		reg [6:0] T;// time interger
        assign led70o= sw[2]? 8'b11000000:led70r;
        assign led7o = sw[2]? 8'b11000000:led7r ;
        always @ (posedge clk_50mhz or posedge sw[2])
        begin
                if(sw[2]) 
					begin
						count <= 26'd0;
						T = 100;
						printer();
					end
                else if (count >= 26'd49999999)
					begin
					
						if(sw[1])
							begin 
							T = (T+1)%100;// way count
							//if (T >= 99) T = 0;
							end
						else
							begin
							T = T-1;//way count
							if (T == 0) T = 100;
							end
						printer();
						count = 0;
					end
				else 
					begin
						count <= count + 1;
					end
        end
        
        task printer ;
			begin
				case(T%10)
					0:	led7r<=8'b11000000;//0	
					1:	led7r<=8'b11111001;//1
					2:	led7r<=8'b10100100;//2
					3:	led7r<=8'b10110000;//3
					4:	led7r<=8'b10011001;//4	
					5:	led7r<=8'b10010010;//5
					6:	led7r<=8'b10000010;//6
					7:	led7r<=8'b11111000;//7
					8:	led7r<=8'b10000000;//8	
					9:	led7r<=8'b10011000;//9
					default	:led7r<=8'b11000000;//0
				endcase
				case((T%100)/10)
					0:	led70r<=8'b11000000;//0	
					1:	led70r<=8'b11111001;//1
					2:	led70r<=8'b10100100;//2
					3:	led70r<=8'b10110000;//3
					4:	led70r<=8'b10011001;//4	
					5:	led70r<=8'b10010010;//5
					6:	led70r<=8'b10000010;//6
					7:	led70r<=8'b11111000;//7
					8:	led70r<=8'b10000000;//8	
					9:	led70r<=8'b10011000;//9
					default	:led70r<=8'b11000000;//0
				endcase
									/*
									8'b11000000;//0
									8'b11111001;//1
									8'b10100100;//2
									8'b10110000;//3
									8'b10011001;//4
									8'b10010010;//5
									8'b10000010;//6
									8'b11111000;//7
									8'b10000000;//8
									8'b10011000;//9
									*/
			end
		endtask
endmodule

module u2(sw,led7a,led7sup, led7b);

input [1:12] sw;
output [7:0] led7a;
output [7:0]led7sup;
output [7:0] led7b;
reg [7:0] led7a;
reg [7:0] led7b;
reg [7:0]led7sup;
reg [5:0] A;
reg [5:0] B;
reg [5:0] C;


always@(sw)
begin
//    we are god
        A[4:0] = sw[3:7];
        B[4:0] = sw[8:12];
//fuck you
		if (A[4]==1'b1)
			A[5] = 1'b1;
		else
			A[5] = 1'b0;
		if (B[4]==1'b1)
			B[5] = 1'b1;
		else
			B[5] = 1'b0;
			
        if (sw[2])
                begin
                if (sw[1])      //add
						C = A + B;
                else    //sub
						begin
						B = ~B+1'b1;
                        C = A + B;
						end
                if (C[5]==1'b1)
                        begin
                        led7sup <= 8'b10111111; //-
                        C = ~C + 1;
                        end
                else
                        led7sup <= 8'b11111111;
                printer();
                        
                end
        else
                begin
                led7sup <= 8'b11111111;
                led7a <= 8'b11111111;
                led7b <= 8'b11111111;
                end
end
        task printer ;
                begin
                        case(C%10)
                                0:      led7a<=8'b11000000;//0     
                                1:      led7a<=8'b11111001;//1
                                2:      led7a<=8'b10100100;//2
                                3:      led7a<=8'b10110000;//3
                                4:      led7a<=8'b10011001;//4     
                                5:      led7a<=8'b10010010;//5
                                6:      led7a<=8'b10000010;//6
                                7:      led7a<=8'b11111000;//7
                                8:      led7a<=8'b10000000;//8     
                                9:      led7a<=8'b10011000;//9
                                default :led7a<=8'b10000000;//8
                        endcase
                        case((C%100)/10)
                                0:      led7b<=8'b11000000;//0     
                                1:      led7b<=8'b11111001;//1
                                2:      led7b<=8'b10100100;//2
                                3:      led7b<=8'b10110000;//3
                                4:      led7b<=8'b10011001;//4     
                                5:      led7b<=8'b10010010;//5
                                6:      led7b<=8'b10000010;//6
                                7:      led7b<=8'b11111000;//7
                                8:      led7b<=8'b10000000;//8     
                                9:      led7b<=8'b10011000;//9
                                default :led7b<=8'b10000000;//8
                        endcase
                end
        endtask
endmodule

module u3(sw,sw2,clk,oLCD_data,olcd_rw,olcd_en,olcd_rs,olcd_on);
	input clk;
	input [0:2]sw;
	input [0:2]sw2;
	output [7:0]oLCD_data;
	output olcd_rw,olcd_en,olcd_rs,olcd_on;
	reg [7:0]LCD_data;                         //lcd data 0~7
	reg lcd_rw,lcd_en,lcd_rs,lcd_on,start,done,howcount; //rw:1read 0write,en:1 enable,rs:1data 0command ,on:1on ,blon:1blon
	reg [0:2]staterr,staterr2;
	reg [28:0]counter,tmptime;
	reg [5:0]which,need;
	reg [9:0]number;
	assign oLCD_data=LCD_data;
	assign olcd_rw=lcd_rw;
	assign olcd_en=lcd_en;
	assign olcd_rs=lcd_rs;
	assign olcd_on=lcd_on;
	initial                                     
	begin
		howcount=1'b0;//往上
		need=6'd0;
		which=6'd1;
		counter<=28'd0;
		lcd_en=1'b0;
		lcd_on=1'b1;
		LCD_data=8'b00000001;
		lcd_rw<=1'b0;
		lcd_rs<=1'b0;
		done=1'b0;
		number=10'd0;
//counter=28'd0;
	end
	always @(posedge clk)
	begin
	if(staterr!=sw || staterr2!=sw2) //sw變化了 
	begin
	staterr2=sw2;
	staterr=sw;
	need=6'd3;
	which=6'd0; //計數器歸零 which表達印到第幾個字
		if(sw==3'b111)//最後一個的話  要初始化number
			number=6'd0;
			done=1'b0;
			counter=28'd0;//把時間歸0  重跑
	end
	else if(which>need)  //done 進入等待
	begin
	done=1'b1;
	end
	else if(which<=need) //還沒寫入need個指令
	begin
		if(counter<=28'd10)
			tmptime=counter; //暫時存起時間
		if(which<2)    //前兩個指令  必為clear 跟00001111
		begin
		lcd_rs=1'b0;   //rs 設為command
		case(which)
			6'd0:	LCD_data=8'b00000001;
			6'd1:	LCD_data=8'b00001111;
			default : LCD_data=8'b00001111;
		endcase
		end         
		else         //which>2  視sw
		begin
			if(sw==3'b000)
			begin
				need=6'd10;
				lcd_rs=1'b1;
				if(sw2==3'b001)	// doggy
				begin
					case(which)
						6'd2:	LCD_data = 8'b01000010;
						6'd3:	LCD_data = 8'b00110000;
						6'd4:	LCD_data = 8'b00110000;
						6'd5:	LCD_data = 8'b00111001;
						6'd6:	LCD_data = 8'b00110000;
						6'd7:	LCD_data = 8'b00110010;
						6'd8:	LCD_data = 8'b00110000;
						6'd9:	LCD_data = 8'b00110101;
						6'd10:	LCD_data = 8'b00110101;
						default:LCD_data=8'b00100000;
					endcase
				end
				else if(sw2==3'b000)	//yen
				begin
					need=6'd18;
					case(which)
						6'd2:	LCD_data = 8'b01000010;
						6'd3:	LCD_data = 8'b00110000;
						6'd4:	LCD_data = 8'b00110000;
						6'd5:	LCD_data = 8'b00111001;
						6'd6:	LCD_data = 8'b00110000;
						6'd7:	LCD_data = 8'b00110010;
						6'd8:	LCD_data = 8'b00110000;
						6'd9:	LCD_data = 8'b00110101;
						6'd10:	LCD_data = 8'b00110011;
						6'd11:	LCD_data = 8'b00100000;	//
						6'd12:	LCD_data = 8'b00100000;	//
						6'd13:	LCD_data = 8'b00100000;	//
						6'd14:	LCD_data = 8'b00100000;	//
						6'd15:	LCD_data = 8'b01011001;	//Y
						6'd16:	LCD_data = 8'b01100101;	//e
						6'd17:	LCD_data = 8'b01101110;	//n
						default:LCD_data=8'b00100000;
					endcase
				end
				else if(sw2==3'b010)	// pig
				begin
					case(which)
						6'd2:	LCD_data = 8'b01000010;
						6'd3:	LCD_data = 8'b00110000;
						6'd4:	LCD_data = 8'b00110000;
						6'd5:	LCD_data = 8'b00111001;
						6'd6:	LCD_data = 8'b00110000;
						6'd7:	LCD_data = 8'b00110010;
						6'd8:	LCD_data = 8'b00110000;
						6'd9:	LCD_data = 8'b00110000;
						6'd10:	LCD_data = 8'b00110101;
						default:LCD_data=8'b00100000;
					endcase
				end
				else if(sw2==3'b011)	//lin tsou
				begin
					case(which)
						6'd2:	LCD_data = 8'b01000010;
						6'd3:	LCD_data = 8'b00110000;
						6'd4:	LCD_data = 8'b00110000;
						6'd5:	LCD_data = 8'b00111001;
						6'd6:	LCD_data = 8'b00110000;
						6'd7:	LCD_data = 8'b00110010;
						6'd8:	LCD_data = 8'b00110000;
						6'd9:	LCD_data = 8'b00110010;
						6'd10:	LCD_data = 8'b00110010;
						default:LCD_data=8'b00100000;
					endcase
				end
				else
				begin
					need=6'd16;
					case(which)
						6'd2:	LCD_data = 8'b01001110;	//N
						6'd3:	LCD_data = 8'b01101111;	//o
						6'd4:	LCD_data = 8'b00100000;	// 
						6'd5:	LCD_data = 8'b01110100;	//t
						6'd6:	LCD_data = 8'b01101000;	//h
						6'd7:	LCD_data = 8'b01101001;	//i
						6'd8:	LCD_data = 8'b01110011;	//s
						6'd9:	LCD_data = 8'b00100000;	//
						6'd10:	LCD_data = 8'b01101101;	//m	
						6'd11:	LCD_data = 8'b01100101;	//e
						6'd12:	LCD_data = 8'b01101101;	//m
						6'd13:	LCD_data = 8'b01100010;	//b
						6'd14:	LCD_data = 8'b01100101;	//e
						6'd15:	LCD_data = 8'b01110010;	//r
						default:LCD_data=8'b00100000;
					endcase
				end
			end
			else if(sw==3'b001)
			begin
				lcd_rs=1'b1; //rs 設為data
				need=6'd10;
				case(which)
					6'd2:	LCD_data=8'b01001000;//H  
					6'd3:	LCD_data=8'b01000101;//E
					6'd4:	LCD_data=8'b01001100;//L
					6'd5:	LCD_data=8'b01001100;//L
					6'd6:	LCD_data=8'b01001111;//O
					6'd7:	LCD_data=8'b00100000;
					6'd8:	LCD_data=8'b01000100;//D
					6'd9:	LCD_data=8'b01010011;//S
					6'd10:	LCD_data=8'b01001100;//L 
					default:LCD_data=8'b00100000;
				endcase
				
			end
			else if(sw==3'b010)//LSD HELLO @line 2
			begin
				lcd_rs=1'b1;
				need=6'd11;
				case(which)
					6'd2:	begin
							lcd_rs=1'b0;
							LCD_data=8'b10101000;//游標換行
							end
					6'd3:	LCD_data=8'b01001100;
					6'd4:	LCD_data=8'b01010011;
					6'd5:	LCD_data=8'b01000100;
					6'd6:	LCD_data=8'b00100000;
					6'd7:	LCD_data=8'b01001111;
					6'd8:	LCD_data=8'b01001100;
					6'd9:	LCD_data=8'b01001100;
					6'd10:	LCD_data=8'b01000101;
					6'd11:	LCD_data=8'b01001000;
					default:LCD_data=8'b00100000;
				endcase
			end
			else if(sw==3'b011)//clear
			begin
			need=6'd2;
			lcd_rs=1'b0;
			LCD_data=8'b00000001;
			end
			else if(sw==3'b100)//all dots
			begin
				need=6'd36;
				if(which==6'd18)
					begin
					lcd_rs=1'b0;
					LCD_data=8'b10101000;//游標換行
					end
				else 
				begin
				lcd_rs=1'b1;
				LCD_data=8'b11111111;
				end
			end
			else if(sw==3'b101)//left->right in line 1
			begin
				need=6'd16;
				lcd_rs=1'b1;
				LCD_data=8'b00100000;
				tmptime=28'd49999999;
			end
			else if(sw==3'b110)//right->left in line 2
			begin
				need=6'd17;
				if(which==6'd2) 
				begin
					lcd_rs=1'b0;
					LCD_data=8'b1001111;//游標換行
				end
				else
				begin
					lcd_rs=1'b0;
					LCD_data=8'b00010000;
					tmptime=28'd49999999;
				end
			end
			
			else if(sw==3'b111)//counter 0x
			begin
				need=6'd50;
				case (which)
					6'd2: LCD_data=8'b10101110;//move to 46
					6'd3: //write 0
					begin 
						lcd_rs=1'b1;
						LCD_data=8'b00110000;//0
					end
					6'd4:	LCD_data=8'b01111000;//x
					6'd5:begin
					lcd_rs=1'b1;
					case (number/10'd16)
						0: LCD_data=8'b00110000;//0
						1: LCD_data=8'b00110001;
						2: LCD_data=8'b00110010;
						3: LCD_data=8'b00110011;
						4: LCD_data=8'b00110100;
						5: LCD_data=8'b00110101;
						6: LCD_data=8'b00110110;
						7: LCD_data=8'b00110111;
						8: LCD_data=8'b00111000;
						9: LCD_data=8'b00111001;
						10: LCD_data=8'b01000001;//A
						11: LCD_data=8'b01000010;//B
						12: LCD_data=8'b01000011;//C
						13: LCD_data=8'b01000100;//D
						14: LCD_data=8'b01000101;//E
						15: LCD_data=8'b01000110;//F
						default: LCD_data=8'b01000111;//GG
					endcase
					end
					6'd6:begin
					lcd_rs=1'b1;
					case(number%10'd16)
						0: LCD_data=8'b00110000;//0
						1: LCD_data=8'b00110001;
						2: LCD_data=8'b00110010;
						3: LCD_data=8'b00110011;
						4: LCD_data=8'b00110100;
						5: LCD_data=8'b00110101;
						6: LCD_data=8'b00110110;
						7: LCD_data=8'b00110111;
						8: LCD_data=8'b00111000;
						9: LCD_data=8'b00111001;
						10: LCD_data=8'b01000001;//A
						11: LCD_data=8'b01000010;//B
						12: LCD_data=8'b01000011;//C
						13: LCD_data=8'b01000100;//D
						14: LCD_data=8'b01000101;//E
						15: LCD_data=8'b01000110;//F
						default: LCD_data=8'b01000111;//GG
					endcase
					end
					6'd7:
					begin//move back to 48
						lcd_rs=1'b0;
						LCD_data=8'b10110000;
						//tmptime=28'd10000000;
						tmptime=28'd25000000;
					end
					6'd8:
					begin
						which=6'd5;
						if(number==10'd255)
							howcount=1'b1;//往下
						else if(number==10'd0)
							howcount=1'b0;
						if(howcount)
							number=number-10'd1;
						else 
							number=number+10'd1;
					end
				endcase
			end
		end
		///以上是八個sw的情況
		if(counter>=tmptime+28'd80000) //  等到40000以上 拉低enable,counter 歸零,which+1 完成此筆swled亮第三顆
		begin 
		lcd_en=1'b0;   
		counter=28'd0;
		which=which+6'd1;
		staterr2=sw2;
		staterr=sw;
		end
		else if(counter>=tmptime+28'd40000)// 等到20000以上  拉高enable
		begin
		lcd_en = 1'b1;
		end

	end
	counter=counter+28'd1;  //時間永遠加一
	end

endmodule

module u45(clk_50mhz, sw, GPIO,oLCD_data,olcd_rw,olcd_en,olcd_rs,olcd_on, led7a, led7b, led7d, led7e, led7f, led7h, led7g);

input [1:15] sw;
input clk_50mhz;
output reg[1:0] GPIO;
output reg [7:0] led7a, led7b, led7d, led7e, led7f, led7h, led7g;
output [7:0]oLCD_data;
output olcd_rw,olcd_en,olcd_rs,olcd_on;
reg [2:0]staterr1,staterr2,staterr3,staterr4;
reg [7:0]LCD_data; 
reg lcd_rw,lcd_en,lcd_rs,lcd_on,start,done,howcount;
reg [16:0] f11, f12;
reg [16:0] f21, f22;
reg [7:0] F1;
reg [7:0] F2;
reg [3:0] C1;
reg [3:0] C2;
reg [16:0] count1;
reg [16:0] count2;
reg [0:14] swQ;
reg [28:0]counter,tmptime;
reg [5:0]which,need;
reg [9:0]number;
reg version;	// 1 for GPIO1, 0 for GPIO2
assign oLCD_data=LCD_data;
assign olcd_rw=lcd_rw;
assign olcd_en=lcd_en;
assign olcd_rs=lcd_rs;
assign olcd_on=lcd_on;

initial 
begin
count1 = 0;
count2 = 0;
version = 0;
lcd_on = 1;
lcd_en = 0;
lcd_rs = 0;
swQ = 0;
end
always @ (sw)
begin
        case(sw[5:7])
                0:      C1 <= 1;
                1:      C1 <= 2;
                2:      C1 <= 3;
                3:      C1 <= 4;
                4:      C1 <= 5;
                5:      C1 <= 6;
                6:      C1 <= 7;
                7:      C1 <= 8;
        endcase
        case(sw[13:15])
                0:      C2 <= 1;
                1:      C2 <= 2;
                2:      C2 <= 3;
                3:      C2 <= 4;
                4:      C2 <= 5;
                5:      C2 <= 6;
                6:      C2 <= 7;
                7:      C2 <= 8;
        endcase
		case(sw[9:11])
                0:      
				begin
                                        F2 <= 1; 
                                        f21 <= 50000;
										case(sw[13:15])
										0:      f22<=5000;
										1:      f22<=10000;
										2:      f22<=15000;
										3:      f22<=20000;
										4:      f22<=25000;
										5:      f22<=30000;
										6:      f22<=35000;
										7:      f22<=40000;
										endcase
                                end
                1:      
                begin
                                        F2 <= 2; 
                                        f21 <= 25000;
										case(sw[13:15])
										0:      f22<=2500;
										1:      f22<=5000;
										2:      f22<=75000;
										3:      f22<=10000;
										4:      f22<=12500;
										5:      f22<=15000;
										6:      f22<=17500;
										7:      f22<=20000;
										endcase
                                end
                2:      
                begin
                                        F2 <= 4; f21 <= 12500;
										case(sw[13:15])
										0:      f22<=1250;
										1:      f22<=2500;
										2:      f22<=3750;
										3:      f22<=5000;
										4:      f22<=6250;
										5:      f22<=7500;
										6:      f22<=8750;
										7:      f22<=10000;
										endcase
                                end
                3:      
                begin
                                        F2 <= 8; f21 <= 6250;
										case(sw[13:15])
										0:      f22<=625;
										1:      f22<=1250;
										2:      f22<=1875;
										3:      f22<=2500;
										4:      f22<=3125;
										5:      f22<=3750;
										6:      f22<=4375;
										7:      f22<=5000;
										endcase
                                end
                4:      
                begin 
                                        F2 <= 16; f21 <= 3125;
										case(sw[13:15])
										0:      f22<=313;
										1:      f22<=625;
										2:      f22<=938;
										3:      f22<=1250;
										4:      f22<=1563;
										5:      f22<=1875;
										6:      f22<=2188;
										7:      f22<=2500;
										endcase
                                end
                5:      
                begin
                                        F2 <= 32; f21 <= 1563;
										case(sw[13:15])
										0:      f22<=156;
										1:      f22<=312;
										2:      f22<=468;
										3:      f22<=624;
										4:      f22<=780;
										5:      f22<=936;
										6:      f22<=1092;
										7:      f22<=1248;
										endcase
                                end
                6:      
                begin
                                        F2 <= 64; f21 <= 781;
										case(sw[13:15])
										0:      f22<=78;
										1:      f22<=156;
										2:      f22<=234;
										3:      f22<=312;
										4:      f22<=390;
										5:      f22<=468;
										6:      f22<=546;
										7:      f22<=624;
										endcase
                                end
                7:      
                begin
                                        F2 <= 128; f21 <= 391;
										case(sw[13:15])
										0:      f22<=39;
										1:      f22<=78;
										2:      f22<=117;
										3:      f22<=156;
										4:      f22<=195;
										5:      f22<=234;
										6:      f22<=273;
										7:      f22<=312;
										endcase
                                end
        endcase
        case(sw[1:3])
                0:      
				begin
                                        F1 <= 1; 
                                        f11 <= 50000;
										case(sw[5:7])
										0:      f12<=5000;
										1:      f12<=10000;
										2:      f12<=15000;
										3:      f12<=20000;
										4:      f12<=25000;
										5:      f12<=30000;
										6:      f12<=35000;
										7:      f12<=40000;
										endcase
                                end
                1:      
                begin
                                        F1 <= 2; 
                                        f11 <= 25000;
										case(sw[5:7])
										0:      f12<=2500;
										1:      f12<=5000;
										2:      f12<=75000;
										3:      f12<=10000;
										4:      f12<=12500;
										5:      f12<=15000;
										6:      f12<=17500;
										7:      f12<=20000;
										endcase
                                end
                2:      
                begin
                                        F1 <= 4; f11 <= 12500;
										case(sw[5:7])
										0:      f12<=1250;
										1:      f12<=2500;
										2:      f12<=3750;
										3:      f12<=5000;
										4:      f12<=6250;
										5:      f12<=7500;
										6:      f12<=8750;
										7:      f12<=10000;
										endcase
                                end
                3:      
                begin
                                        F1 <= 8; f11 <= 6250;
										case(sw[5:7])
										0:      f12<=625;
										1:      f12<=1250;
										2:      f12<=1875;
										3:      f12<=2500;
										4:      f12<=3125;
										5:      f12<=3750;
										6:      f12<=4375;
										7:      f12<=5000;
										endcase
                                end
                4:      
                begin 
                                        F1 <= 16; f11 <= 3125;
										case(sw[5:7])
										0:      f12<=313;
										1:      f12<=625;
										2:      f12<=938;
										3:      f12<=1250;
										4:      f12<=1563;
										5:      f12<=1875;
										6:      f12<=2188;
										7:      f12<=2500;
										endcase
                                end
                5:      
                begin
                                        F1 <= 32; f11 <= 1563;
										case(sw[5:7])
										0:      f12<=156;
										1:      f12<=312;
										2:      f12<=468;
										3:      f12<=624;
										4:      f12<=780;
										5:      f12<=936;
										6:      f12<=1092;
										7:      f12<=1248;
										endcase
                                end
                6:      
                begin
                                        F1 <= 64; f11 <= 781;
										case(sw[5:7])
										0:      f12<=78;
										1:      f12<=156;
										2:      f12<=234;
										3:      f12<=312;
										4:      f12<=390;
										5:      f12<=468;
										6:      f12<=546;
										7:      f12<=624;
										endcase
                                end
                7:      
                begin
                                        F1 <= 128; f11 <= 391;
										case(sw[5:7])
										0:      f12<=39;
										1:      f12<=78;
										2:      f12<=117;
										3:      f12<=156;
										4:      f12<=195;
										5:      f12<=234;
										6:      f12<=273;
										7:      f12<=312;
										endcase
                                end
        endcase
        printer();//printout
end

always @ (posedge clk_50mhz)
begin
        if (sw != swQ) 
                begin
                        GPIO[0] = 1'b1;
                        GPIO[1] = 1'b1;
                        count1 = 0;
                                                count2 = 0;
                        swQ = sw;
                end
        else; //ledr[2] = 1;
        if( count1  >= f12  )
                begin
                GPIO[0] = 1'b0;
                end
        else;
        if( count2  >= f22  )
                GPIO[1] = 1'b0;
        else;
        if( count1 >= f11 )
                begin
                GPIO[0] = 1'b1;
                                count1 = 0;
                end
        else;
        if( count2 >= f21 )
                        begin
            GPIO[1] = 1'b1;
                        count2 = 0;
                        end
        else;
        count1 = count1 + 1;
                count2 = count2 + 1;
		// version begin
	if(staterr1!=sw[1:3]||staterr2!=sw[5:7]) //sw變化了 
	begin
	staterr1=sw[1:3];
	staterr2=sw[5:7];
	version = 1'b1;
	counter = 28'd0;
	//ledr[1]=1'b1;
	need=6'd30;
	which=6'd0; //計數器歸零 which表達印到第幾個字
	end
	else if(staterr3!=sw[9:11]||staterr4!=sw[13:15])
	begin
	staterr3=sw[9:11];
	staterr4=sw[13:15];
	version = 1'b0;
	counter = 28'd0;
	//ledr[1]=1'b1;
	need=6'd30;
	which=6'd0; //計數器歸零 which表達印到第幾個字
	end
	else;      
	if(which>need)  //done 進入等待
	begin
	//ledr[1]=1'b1;
	done=1'b1;
	end
	else if(which<=need) //還沒寫入need個指令
	begin
	//ledr[0]=version;
	//ledr[1]=1'b1;	//亮111
		if(counter<=28'd10)
			tmptime=counter; //暫時存起時間
		else;
		if(which<2)    //前兩個指令  必為clear 跟00001111
		begin
		lcd_rs=1'b0;   //rs 設為command
		case(which)
			6'd0:	
			begin
			LCD_data=8'b00000001;
			end
			6'd1:	LCD_data=8'b00001111;
			default : LCD_data=8'b00001111;
		endcase
		end         
		else         //which>2  視sw
		begin
			//need=28;
			lcd_rs=1'b1;
			case(which)
				6'd2:	LCD_data = 8'b01000110;	//F
				6'd3:	LCD_data = 8'b01010010;	//R
				6'd4:	LCD_data = 8'b01000101;	//E
				6'd5:	LCD_data = 8'b01010001;	//Q
				6'd6:	LCD_data = 8'b00111010;	//:
				6'd7:
				begin
					if(version == 1)
					begin
						if(sw[1:3]==7)
							LCD_data = 8'b00110001;//1
						else
							LCD_data = 8'b00100000;// 
					end
					else
					begin
						if(sw[9:11]==7)
							LCD_data = 8'b00110001;//1
						else
							LCD_data = 8'b00100000;// 
					end
				end
				6'd8:
				begin
					if(version == 1)
					case(sw[1:3])
						4:	LCD_data = 8'b00110001;//1
						5:	LCD_data = 8'b00110011;//3
						6:	LCD_data = 8'b00110110;//6
						7:	LCD_data = 8'b00110010;//2
						default:	LCD_data = 8'b00100000;
					endcase
					else
					case(sw[9:11])
						4:	LCD_data = 8'b00110001;//1
						5:	LCD_data = 8'b00110011;//3
						6:	LCD_data = 8'b00110110;//6
						7:	LCD_data = 8'b00110010;//2
						default:	LCD_data = 8'b00100000;
					endcase
				end
				6'd9:	
				begin
					if(version == 1)
					case(sw[1:3])
						0:	LCD_data = 8'b00110001;//1
						1:	LCD_data = 8'b00110010;//2
						2:	LCD_data = 8'b00110100;//4
						3:	LCD_data = 8'b00111000;//8
						4:	LCD_data = 8'b00110110;//6
						5:	LCD_data = 8'b00110010;//2
						6:	LCD_data = 8'b00110100;//4
						7:	LCD_data = 8'b00111000;//8
						default:	LCD_data = 8'b00111000;//8
					endcase
					else
					case(sw[9:11])
						0:	LCD_data = 8'b00110001;//1
						1:	LCD_data = 8'b00110010;//2
						2:	LCD_data = 8'b00110100;//4
						3:	LCD_data = 8'b00111000;//8
						4:	LCD_data = 8'b00110110;//6
						5:	LCD_data = 8'b00110010;//2
						6:	LCD_data = 8'b00110100;//4
						7:	LCD_data = 8'b00111000;//8
						default:	LCD_data = 8'b00111000;//8
					endcase
				end
				6'd10:	LCD_data = 8'b00110000;	//0
				6'd11:  LCD_data = 8'b00110000;	//0
				6'd12:	LCD_data = 8'b01001000;	//H
				6'd13:	LCD_data = 8'b01111010;	//z
				6'd14:	
				begin
					lcd_rs = 1'b0;
					LCD_data=8'b10101000;//游標換行
				end
				6'd15:
				begin
					lcd_rs = 1'b1;
					LCD_data = 8'b01000100;	//D
				end
				6'd16:	LCD_data = 8'b01010101;	//U
				6'd17:	LCD_data = 8'b01010100;	//T
				6'd18:	LCD_data = 8'b01011001;	//Y
				6'd19:	LCD_data = 8'b00100000;	//
				6'd20:	LCD_data = 8'b01000011;	//C
				6'd21:	LCD_data = 8'b01011001;	//Y
				6'd22:	LCD_data = 8'b01000011;	//C
				6'd23:	LCD_data = 8'b01001100;	//L
				6'd24:	LCD_data = 8'b01000101;	//E
				6'd25:	LCD_data = 8'b00111010;	//:
				6'd26:	
				begin
					if(version)
					case(sw[5:7])
						0:	LCD_data = 8'b00110001;	//1
						1:	LCD_data = 8'b00110010;//2
						2:	LCD_data = 8'b00110011;//3
						3:	LCD_data = 8'b00110100;//4
						4:	LCD_data = 8'b00110101;//5
						5:	LCD_data = 8'b00110110;//6
						6:	LCD_data = 8'b00110111;//7
						7:	LCD_data = 8'b00111000;//8
						default:	LCD_data = 8'b00111000;//8
					endcase
					else
					case(sw[13:15])
						0:	LCD_data = 8'b00110001;	//1
						1:	LCD_data = 8'b00110010;//2
						2:	LCD_data = 8'b00110011;//3
						3:	LCD_data = 8'b00110100;//4
						4:	LCD_data = 8'b00110101;//5
						5:	LCD_data = 8'b00110110;//6
						6:	LCD_data = 8'b00110111;//7
						7:	LCD_data = 8'b00111000;//8
						default:	LCD_data = 8'b00111000;//8
					endcase
				end
				6'd27:	LCD_data = 8'b00110000;//0
				6'd28:	LCD_data = 8'b00100101;	//%
				default:LCD_data = 8'b00100000;
			endcase
		end
		///以上是八個sw的情況
		if(counter>=tmptime+28'd80000) //  等到40000以上 拉低enable,counter 歸零,which+1 完成此筆swled亮第三顆
		begin 
		lcd_en=1'b0;   
		counter=28'd0;
		if(which<=need)
			which=which+6'd1;
/*		staterr1=sw[2:0];
		staterr2=sw[6:4];
		staterr3=sw[10:8];
		staterr4=sw[14:12];*/
		end
		else if(counter>=tmptime+28'd40000)// 等到20000以上  拉高enable
		begin
			lcd_en=1'b1;
			counter = counter + 28'd1;
		end
		else
			counter = counter + 28'd1;
	end
	//counter=counter+28'd1;
end

task printer ;
    begin
        case(C1)     
            1:  led7a<=8'b11111001;//1
            2:  led7a<=8'b10100100;//2
            3:  led7a<=8'b10110000;//3
            4:  led7a<=8'b10011001;//4     
            5:  led7a<=8'b10010010;//5
            6:  led7a<=8'b10000010;//6
            7:  led7a<=8'b11111000;//7
            8:  led7a<=8'b10000000;//8     
            9:  led7a<=8'b10011000;//9
            default :led7a<=8'b10000000;//8
        endcase
        led7b<=8'b11000000;//0
        case(F1)
            128:  
            begin
            led7d<=8'b11111001; led7e<=8'b10100100; led7f<=8'b10000000;//128
            end
            64:
            begin
                        led7d<=8'b11111111; led7e<=8'b10000010; led7f<=8'b10011001;//64 
            end
            32:
            begin
                        led7d<=8'b11111111; led7e<=8'b10110000; led7f<=8'b10100100;//32
            end
            16:
            begin
                        led7d<=8'b11111111; led7e<=8'b11111001; led7f<=8'b10000010;//16     
            end
            8: 
            begin
            led7d<=8'b11111111; led7e<=8'b11111111; led7f<=8'b10000000;//8 
                        end
                        4: 
                        begin
                        led7d<=8'b11111111; led7e<=8'b11111111; led7f<=8'b10011001;//4
            end
            2: 
            begin
            led7d<=8'b11111111; led7e<=8'b11111111; led7f<=8'b10100100;//2
                        end
                        1: 
                        begin
                        led7d<=8'b11111111; led7e<=8'b11111111; led7f<=8'b11111001;//1
            end
            default : 
            begin
            led7d<=8'b11111111; led7e<=8'b11111111; led7f<=8'b11111111;//nothing
                        end
        endcase
        led7h<=8'b11000000;//0
        led7g<=8'b11000000;//0
        end
    endtask

endmodule




module main(sw,key,clk_50mhz,hex0,hex1,hex2,hex3,hex4,hex5,hex6,hex7, LCD_data, lcd_rw, lcd_en, lcd_rs, lcd_on, GPIO, ledr);
	//reg [3:0]key; 				 			  //button 0~3   Normally high
	input [1:17]sw; 							  //switch 0~15. According to pdf plz use sw from sw[1]
	input clk_50mhz; 							  //for clock
	input [3:0]key;
	output reg[5:0]ledr = 6'b000000;
	output [7:0]hex0, hex1, hex2, hex3, hex4, hex5, hex6, hex7;//,seg2,seg3;//7-segment 0~7
	reg [7:0] hex0=255, hex1=255, hex2=255, hex3=255, hex4=255, hex5=255, hex6=255, hex7=255;//7-segment 0~7                
	output reg [7:0]LCD_data;                         //lcd data 0~7
	reg [2:0] op_num = 1, key_num = 0, tmp;
	reg [25:0] T = 0;
	reg key_stroke = 0, to_clean = 0;
	output reg lcd_rw,lcd_en,lcd_rs,lcd_on;  //rw:1read 0write,en:1 enable,rs:1data 0command ,on:1on ,blon:1blon
	output reg [1:0]GPIO;                             //GPIO 0~1
	wire [7:0] hex6u1, hex7u1, hex4u2, hex5u2, hex6u2, hex0u4, hex1u4, hex2u4, hex3u4, hex4u4, hex6u4, hex7u4;
	wire [7:0] lcd_data_u3, lcd_data_u4;
	wire lcd_en_u3, lcd_en_u4, lcd_on_u3, lcd_on_u4, lcd_rs_u3, lcd_rs_u4, lcd_rw_u3, lcd_rw_u4;
	wire [2:0] gpio;
	u1 Utility1(.clk_50mhz(clk_50mhz),.sw(sw[1:2]),.led70o(hex7u1),.led7o(hex6u1));
	u2 Utility2(.sw(sw[1:12]), .led7a(hex4u2), .led7sup(hex6u2), .led7b(hex5u2));
	u3 Utility3(.sw(sw[1:3]), .sw2(sw[4:6]), .clk(clk_50mhz), .oLCD_data(lcd_data_u3), 
	.olcd_rw(lcd_rw_u3), .olcd_en(lcd_en_u3), .olcd_rs(lcd_rs_u3), .olcd_on(lcd_on_u3));
	u45 Utility45(.clk_50mhz(clk_50mhz), .sw(sw[1:15]), .GPIO(gpio),.oLCD_data(lcd_data_u4),
		.olcd_rw(lcd_rw_u4),.olcd_en(lcd_en_u4),.olcd_rs(lcd_rs_u4), .olcd_on(lcd_on_u4), .led7a(hex7u4), 
		.led7b(hex6u4), .led7d(hex4u4), .led7e(hex3u4), .led7f(hex2u4), .led7h(hex1u4), .led7g(hex0u4));
	initial
	begin
	ledr = 6'b000000;
	op_num = 0;
	hex0 = 255;
	hex1 = 255;
	hex2 = 255;
	hex3 = 255;
	hex4 = 255;
	hex5 = 255;
	hex6 = 255;
	hex7 = 255;
	//T = 0;
	end
	always @ (posedge clk_50mhz )
	begin
	// doing debouncing
		if (key_stroke) T = T+1;
		else if (to_clean)
			begin
			if (T < 10)
				begin
				clean();
				LCD_data = 8'b00001000;
				lcd_rs = 0;
				lcd_en = 0;
				lcd_on = 1;
				end
			else if (T >= 20000) 
				begin
				to_clean = 0;
				lcd_en = 0;
				end
			else if (T >= 10000) lcd_en = 1;
			else;
			T = T + 1;
			end
		else ;
		if (T >= 50000 && key_stroke) 
			begin
				to_clean = 1;
				T = 0;
				key_stroke = 0;
				if (key_num != 4) op_num = key_num;
				else if (op_num == 4) op_num = 5;
				else op_num = 4;
			end
		if (key[0] == 0) 
			begin
			key_num = 1;
			key_stroke = 1;
			T = 26'd0;
			end
		else if (key[1] == 0) 
			begin
			key_num = 2;
			key_stroke = 1;
			T = 26'd0;
			end
		else if (key[2] == 0) 
			begin
			key_num = 3;
			key_stroke = 1;
			T = 26'd0;
			end
		else if (key[3] == 0) 
			begin
			key_num = 4;
			key_stroke = 1;
			T = 26'd0;
			end
		else;
		if (tmp != op_num) 
		begin
			tmp = op_num;
			ledr = 6'b000000;
			ledr[op_num] = 1;
		end
		else;
		hex5 = 255;
		if(op_num == 1 && to_clean == 0)
			begin
			hex7<=hex7u1;
			hex6<=hex6u1;
			end
		else if (op_num == 2 && to_clean == 0)
			begin
			hex6<=hex6u2;
			hex5<=hex5u2;
			hex4<=hex4u2;
			end
		else  if (op_num == 3 && to_clean == 0)
			begin
			LCD_data = lcd_data_u3;
			lcd_rw = lcd_rw_u3;
			lcd_en = lcd_en_u3;
			lcd_rs = lcd_rs_u3;
			lcd_on = lcd_on_u3;
			end
		else if (op_num == 4 && to_clean == 0)
			begin
			GPIO = gpio;
			LCD_data = lcd_data_u4;
			lcd_rw = lcd_rw_u4;
			lcd_en = lcd_en_u4;
			lcd_rs = lcd_rs_u4;
			lcd_on = lcd_on_u4;
			end
		else if (op_num == 5 && to_clean == 0) 
			begin
			clean();
			GPIO = gpio;
			hex7<=hex7u4;
			hex6<=hex6u4;
			hex4<=hex4u4;
			hex3<=hex3u4;
			hex2<=hex2u4;
			hex1<=hex1u4;
			hex0<=hex0u4;
			end
		else;
	end
	task clean();
	begin
		GPIO = 2'b00;
		hex0 = 255;
		hex1 = 255;
		hex2 = 255;
		hex3 = 255;
		hex4 = 255;
		hex5 = 255;
		hex6 = 255;
		hex7 = 255;
	end
	endtask
endmodule
