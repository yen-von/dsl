module u1(clk_50mhz, sw, led70o,led7o);
		input clk_50mhz;
        input [1:0]sw;
        output [6:0]led70o;
        output [6:0]led7o;
        reg [6:0]led70r;
        reg [6:0]led7r;
        reg r;// to  remenber the state of counting way
        reg [25:0] count;
        reg a=0;
		reg [6:0] T;// time interger
        assign led70o= sw[1]? 7'b1000000:led70r;
        assign led7o = sw[1]? 7'b1000000:led7r ;
        always @ (posedge clk_50mhz or posedge sw[1])
        begin
                if(sw[1]) 
					begin
                        r=sw[0]; //high count,low countdown
						count <= 26'd0;
						T <= 100;
						printer();
					end
                else if (count >= 26'd49999999)
					begin
						if(r)
							begin 
							T <= (T+1)%100;// way count
							if (T >= 99) T <= 0;
							end
						else
							begin
							T <= T-1;//way count
							if (T == 1) T <= 100;
							end
                        printer() ;
						count = 0;
					end
				else 
					begin
						count <= count + 1;
					end
        end
        
        task printer ;
			begin
				case(T%10)
					0:	led7r<=7'b1000000;//0	
					1:	led7r<=7'b1111001;//1
					2:	led7r<=7'b0100100;//2
					3:	led7r<=7'b0110000;//3
					4:	led7r<=7'b0011001;//4	
					5:	led7r<=7'b0010010;//5
					6:	led7r<=7'b0000010;//6
					7:	led7r<=7'b1111000;//7
					8:	led7r<=7'b0000000;//8	
					9:	led7r<=7'b0011000;//9
					default	:led7r<=7'b0000000;//8
				endcase
				case((T%100)/10)
					0:	led70r<=7'b1000000;//0	
					1:	led70r<=7'b1111001;//1
					2:	led70r<=7'b0100100;//2
					3:	led70r<=7'b0110000;//3
					4:	led70r<=7'b0011001;//4	
					5:	led70r<=7'b0010010;//5
					6:	led70r<=7'b0000010;//6
					7:	led70r<=7'b1111000;//7
					8:	led70r<=7'b0000000;//8	
					9:	led70r<=7'b0011000;//9
					default	:led70r<=7'b0000000;//8
				endcase
									/*
									7'b1000000;//0
									7'b1111001;//1
									7'b0100100;//2
									7'b0110000;//3
									7'b0011001;//4
									7'b0010010;//5
									7'b0000010;//6
									7'b1111000;//7
									7'b0000000;//8
									7'b0011000;//9
									*/
			end
		endtask
endmodule
