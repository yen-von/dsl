module u5(clk_50mhz, sw, GPIO, led7a, led7b, led7d, led7e, led7f, led7g, led7h, ledr);

input [14:0] sw;
input clk_50mhz;
output reg[1:0] GPIO;
output reg [3:0] ledr;
reg [16:0] f11;
reg [16:0] f21;
reg [16:0] f12;
reg [16:0] f22;
reg [7:0] F1;
reg [7:0] F2;
reg [3:0] C1;
reg [3:0] C2;
reg [16:0] count1;
reg [16:0] count2;
reg [14:0] swQ;
output reg[7:0] led7a;
output reg[7:0] led7b;
output reg[7:0] led7d;
output reg[7:0] led7e;
output reg[7:0] led7f;
output reg[7:0] led7g;
output reg[7:0] led7h;
initial 
begin
count1 = 0;
count2 = 0;
ledr = 4'b0111;
swQ = 0;
end
always @ (sw)
begin
        case(sw[6:4])
                0:      C1 <= 1;
                1:      C1 <= 2;
                2:      C1 <= 3;
                3:      C1 <= 4;
                4:      C1 <= 5;
                5:      C1 <= 6;
                6:      C1 <= 7;
                7:      C1 <= 8;
        endcase
        case(sw[14:12])
                0:      C2 <= 1;
                1:      C2 <= 2;
                2:      C2 <= 3;
                3:      C2 <= 4;
                4:      C2 <= 5;
                5:      C2 <= 6;
                6:      C2 <= 7;
                7:      C2 <= 8;
        endcase
		case(sw[10:8])
                0:      
				begin
                                        F2 <= 1; 
                                        f21 <= 50000;
										case(sw[14:12])
										0:      f22<=5000;
										1:      f22<=10000;
										2:      f22<=15000;
										3:      f22<=20000;
										4:      f22<=25000;
										5:      f22<=30000;
										6:      f22<=35000;
										7:      f22<=40000;
										endcase
                                end
                1:      
                begin
                                        F2 <= 2; 
                                        f21 <= 25000;
										case(sw[14:12])
										0:      f22<=2500;
										1:      f22<=5000;
										2:      f22<=75000;
										3:      f22<=10000;
										4:      f22<=12500;
										5:      f22<=15000;
										6:      f22<=17500;
										7:      f22<=20000;
										endcase
                                end
                2:      
                begin
                                        F2 <= 4; f21 <= 12500;
										case(sw[14:12])
										0:      f22<=1250;
										1:      f22<=2500;
										2:      f22<=3750;
										3:      f22<=5000;
										4:      f22<=6250;
										5:      f22<=7500;
										6:      f22<=8750;
										7:      f22<=10000;
										endcase
                                end
                3:      
                begin
                                        F2 <= 8; f21 <= 6250;
										case(sw[14:12])
										0:      f22<=625;
										1:      f22<=1250;
										2:      f22<=1875;
										3:      f22<=2500;
										4:      f22<=3125;
										5:      f22<=3750;
										6:      f22<=4375;
										7:      f22<=5000;
										endcase
                                end
                4:      
                begin 
                                        F2 <= 16; f21 <= 3125;
										case(sw[14:12])
										0:      f22<=313;
										1:      f22<=625;
										2:      f22<=938;
										3:      f22<=1250;
										4:      f22<=1563;
										5:      f22<=1875;
										6:      f22<=2188;
										7:      f22<=2500;
										endcase
                                end
                5:      
                begin
                                        F2 <= 32; f21 <= 1563;
										case(sw[14:12])
										0:      f22<=156;
										1:      f22<=312;
										2:      f22<=468;
										3:      f22<=624;
										4:      f22<=780;
										5:      f22<=936;
										6:      f22<=1092;
										7:      f22<=1248;
										endcase
                                end
                6:      
                begin
                                        F2 <= 64; f21 <= 781;
										case(sw[14:12])
										0:      f22<=78;
										1:      f22<=156;
										2:      f22<=234;
										3:      f22<=312;
										4:      f22<=390;
										5:      f22<=468;
										6:      f22<=546;
										7:      f22<=624;
										endcase
                                end
                7:      
                begin
                                        F2 <= 128; f21 <= 391;
										case(sw[14:12])
										0:      f22<=39;
										1:      f22<=78;
										2:      f22<=117;
										3:      f22<=156;
										4:      f22<=195;
										5:      f22<=234;
										6:      f22<=273;
										7:      f22<=312;
										endcase
                                end
        endcase
        case(sw[2:0])
                0:      
				begin
                                        F1 <= 1; 
                                        f11 <= 50000;
										case(sw[6:4])
										0:      f12<=5000;
										1:      f12<=10000;
										2:      f12<=15000;
										3:      f12<=20000;
										4:      f12<=25000;
										5:      f12<=30000;
										6:      f12<=35000;
										7:      f12<=40000;
										endcase
                                end
                1:      
                begin
                                        F1 <= 2; 
                                        f11 <= 25000;
										case(sw[6:4])
										0:      f12<=2500;
										1:      f12<=5000;
										2:      f12<=75000;
										3:      f12<=10000;
										4:      f12<=12500;
										5:      f12<=15000;
										6:      f12<=17500;
										7:      f12<=20000;
										endcase
                                end
                2:      
                begin
                                        F1 <= 4; f11 <= 12500;
										case(sw[6:4])
										0:      f12<=1250;
										1:      f12<=2500;
										2:      f12<=3750;
										3:      f12<=5000;
										4:      f12<=6250;
										5:      f12<=7500;
										6:      f12<=8750;
										7:      f12<=10000;
										endcase
                                end
                3:      
                begin
                                        F1 <= 8; f11 <= 6250;
										case(sw[6:4])
										0:      f12<=625;
										1:      f12<=1250;
										2:      f12<=1875;
										3:      f12<=2500;
										4:      f12<=3125;
										5:      f12<=3750;
										6:      f12<=4375;
										7:      f12<=5000;
										endcase
                                end
                4:      
                begin 
                                        F1 <= 16; f11 <= 3125;
										case(sw[6:4])
										0:      f12<=313;
										1:      f12<=625;
										2:      f12<=938;
										3:      f12<=1250;
										4:      f12<=1563;
										5:      f12<=1875;
										6:      f12<=2188;
										7:      f12<=2500;
										endcase
                                end
                5:      
                begin
                                        F1 <= 32; f11 <= 1563;
										case(sw[6:4])
										0:      f12<=156;
										1:      f12<=312;
										2:      f12<=468;
										3:      f12<=624;
										4:      f12<=780;
										5:      f12<=936;
										6:      f12<=1092;
										7:      f12<=1248;
										endcase
                                end
                6:      
                begin
                                        F1 <= 64; f11 <= 781;
										case(sw[6:4])
										0:      f12<=78;
										1:      f12<=156;
										2:      f12<=234;
										3:      f12<=312;
										4:      f12<=390;
										5:      f12<=468;
										6:      f12<=546;
										7:      f12<=624;
										endcase
                                end
                7:      
                begin
                                        F1 <= 128; f11 <= 391;
										case(sw[6:4])
										0:      f12<=39;
										1:      f12<=78;
										2:      f12<=117;
										3:      f12<=156;
										4:      f12<=195;
										5:      f12<=234;
										6:      f12<=273;
										7:      f12<=312;
										endcase
                                end
        endcase
        printer();//printout
end

always @ (posedge clk_50mhz)
begin
        if (sw != swQ) 
                begin
                        GPIO[0] = 1'b1;
                        GPIO[1] = 1'b1;
                        count1 = 0;
                                                count2 = 0;
                        ledr[2] = 1'b0;
                        swQ = sw;
                end
        else; //ledr[2] = 1;
        if( count1  >= f12  )
                begin
                GPIO[0] = 1'b0;
                ledr[3] = 1'b0;
                end
        else;
        if( count2  >= f22  )
                GPIO[1] = 1'b0;
        else;
        if( count1 >= f11 )
                begin
                GPIO[0] = 1'b1;
                ledr[3] = 1'b1;
                                count1 = 0;
                end
        else;
        if( count2 >= f21 )
                        begin
            GPIO[1] = 1'b1;
                        count2 = 0;
                        end
        else;
        count1 = count1 + 1;
                count2 = count2 + 1;
end
        task printer ;
    begin
        case(C1)     
            1:  led7a<=8'b11111001;//1
            2:  led7a<=8'b10100100;//2
            3:  led7a<=8'b10110000;//3
            4:  led7a<=8'b10011001;//4     
            5:  led7a<=8'b10010010;//5
            6:  led7a<=8'b10000010;//6
            7:  led7a<=8'b11111000;//7
            8:  led7a<=8'b10000000;//8     
            9:  led7a<=8'b10011000;//9
            default :led7a<=8'b10000000;//8
        endcase
        led7b<=8'b11000000;//0
        case(F1)
            128:  
            begin
            led7d<=8'b11111001; led7e<=8'b10100100; led7f<=8'b10000000;//128
            end
            64:
            begin
                        led7d<=8'b11111111; led7e<=8'b10000010; led7f<=8'b10011001;//64 
            end
            32:
            begin
                        led7d<=8'b11111111; led7e<=8'b10110000; led7f<=8'b10100100;//32
            end
            16:
            begin
                        led7d<=8'b11111111; led7e<=8'b11111001; led7f<=8'b10000010;//16     
            end
            8: 
            begin
            led7d<=8'b11111111; led7e<=8'b11111111; led7f<=8'b10000000;//8 
                        end
                        4: 
                        begin
                        led7d<=8'b11111111; led7e<=8'b11111111; led7f<=8'b10011001;//4
            end
            2: 
            begin
            led7d<=8'b11111111; led7e<=8'b11111111; led7f<=8'b10100100;//2
                        end
                        1: 
                        begin
                        led7d<=8'b11111111; led7e<=8'b11111111; led7f<=8'b11111001;//1
            end
            default : 
            begin
            led7d<=8'b11111111; led7e<=8'b11111111; led7f<=8'b11111111;//nothing
                        end
        endcase
        led7h<=8'b11000000;//0
        led7g<=8'b11000000;//0
        end
    endtask
endmodule
