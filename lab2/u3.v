module u3(sw,sw2,clk,oLCD_data,olcd_rw,olcd_en,olcd_rs,olcd_on,olcd_blon,led,led2,led3);
	input clk;
	input [2:0]sw;
	input [2:0]sw2;
	output [7:0]oLCD_data;
	output olcd_rw,olcd_en,olcd_rs,olcd_on,olcd_blon,led,led2,led3;
	reg led,led2,led3;
	reg [7:0]LCD_data;                         //lcd data 0~7
	reg lcd_rw,lcd_en,lcd_rs,lcd_on,lcd_blon,start,done,howcount; //rw:1read 0write,en:1 enable,rs:1data 0command ,on:1on ,blon:1blon
	reg [2:0]staterr,staterr2;
	reg [28:0]counter,tmptime;
	reg [5:0]which,need;
	reg [9:0]number;
	assign oLCD_data=LCD_data;
	assign olcd_rw=lcd_rw;
	assign olcd_en=lcd_en;
	assign olcd_rs=lcd_rs;
	assign olcd_on=lcd_on;
	assign olcd_blon=lcd_blon;
	initial                                     
	begin
		howcount=1'b0;//往上
		need=6'd0;
		which=6'd1;
		counter<=28'd0;
		lcd_en=1'b0;
		lcd_on=1'b1;
		lcd_blon=1'b1;
		LCD_data=8'b00000001;
		lcd_rw<=1'b0;
		lcd_rs<=1'b0;
		led=1'b0;
		led2=1'b0;
		led3=1'b0;
		done=1'b0;
		number=10'd0;
//counter=28'd0;
	end
	always @(posedge clk)
	begin
		case(which)
						6'd2:	LCD_data = 8'b01000010;
						6'd3:	LCD_data = 8'b00110000;
						6'd4:	LCD_data = 8'b00110000;
						6'd5:	LCD_data = 8'b00111001;
						6'd6:	LCD_data = 8'b00110000;
						6'd7:	LCD_data = 8'b00110010;
						6'd8:	LCD_data = 8'b00110000;
						6'd9:	LCD_data = 8'b00110101;
						6'd10:	LCD_data = 8'b00110101;
						default:LCD_data=8'b00100000;
					endcase
	end
			
	/*always @(posedge clk)
	begin
	if(staterr!=sw || staterr2!=sw2) //sw變化了 
	begin
	staterr2=sw2;
	staterr=sw;
	led=1'b1;
	led2=1'b1;
	led3=1'b0;	
	need=6'd3;
	which=6'd0; //計數器歸零 which表達印到第幾個字
		if(sw==3'b111)//最後一個的話  要初始化number
			number=6'd0;
			done=1'b0;
			counter=28'd0;//把時間歸0  重跑
	end
	else if(which>need)  //done 進入等待
	begin
	led=1'b1;
	led2=1'b0;
	led3=1'b0;  //亮100
	done=1'b1;
	end
	else if(which<=need) //還沒寫入need個指令
	begin
	led=1'b1;
	led2=1'b1;
	led3=1'b1;	//亮111
		if(counter<=28'd10)
			tmptime=counter; //暫時存起時間
		if(which<2)    //前兩個指令  必為clear 跟00001111
		begin
		lcd_rs=1'b0;   //rs 設為command
		case(which)
			6'd0:	LCD_data=8'b00000001;
			6'd1:	LCD_data=8'b00001111;
			default : LCD_data=8'b00001111;
		endcase
		end         
		else         //which>2  視sw
		begin
			if(sw==3'b000)
			begin
				need=6'd10;
				lcd_rs=1'b1;
				if(sw2==3'b001)	// doggy
				begin
					case(which)
						6'd2:	LCD_data = 8'b01000010;
						6'd3:	LCD_data = 8'b00110000;
						6'd4:	LCD_data = 8'b00110000;
						6'd5:	LCD_data = 8'b00111001;
						6'd6:	LCD_data = 8'b00110000;
						6'd7:	LCD_data = 8'b00110010;
						6'd8:	LCD_data = 8'b00110000;
						6'd9:	LCD_data = 8'b00110101;
						6'd10:	LCD_data = 8'b00110101;
						default:LCD_data=8'b00100000;
					endcase
				end
				else if(sw2==3'b000)	//yen
				begin
					need=6'd18;
					case(which)
						6'd2:	LCD_data = 8'b01000010;
						6'd3:	LCD_data = 8'b00110000;
						6'd4:	LCD_data = 8'b00110000;
						6'd5:	LCD_data = 8'b00111001;
						6'd6:	LCD_data = 8'b00110000;
						6'd7:	LCD_data = 8'b00110010;
						6'd8:	LCD_data = 8'b00110000;
						6'd9:	LCD_data = 8'b00110101;
						6'd10:	LCD_data = 8'b00110011;
						6'd11:	LCD_data = 8'b00100000;	//
						6'd12:	LCD_data = 8'b00100000;	//
						6'd13:	LCD_data = 8'b00100000;	//
						6'd14:	LCD_data = 8'b00100000;	//
						6'd15:	LCD_data = 8'b01011001;	//Y
						6'd16:	LCD_data = 8'b01100101;	//e
						6'd17:	LCD_data = 8'b01101110;	//n
						default:LCD_data=8'b00100000;
					endcase
				end
				else if(sw2==3'b010)	// pig
				begin
					case(which)
						6'd2:	LCD_data = 8'b01000010;
						6'd3:	LCD_data = 8'b00110000;
						6'd4:	LCD_data = 8'b00110000;
						6'd5:	LCD_data = 8'b00111001;
						6'd6:	LCD_data = 8'b00110000;
						6'd7:	LCD_data = 8'b00110010;
						6'd8:	LCD_data = 8'b00110000;
						6'd9:	LCD_data = 8'b00110000;
						6'd10:	LCD_data = 8'b00110101;
						default:LCD_data=8'b00100000;
					endcase
				end
				else if(sw2==3'b011)	//lin tsou
				begin
					case(which)
						6'd2:	LCD_data = 8'b01000010;
						6'd3:	LCD_data = 8'b00110000;
						6'd4:	LCD_data = 8'b00110000;
						6'd5:	LCD_data = 8'b00111001;
						6'd6:	LCD_data = 8'b00110000;
						6'd7:	LCD_data = 8'b00110010;
						6'd8:	LCD_data = 8'b00110000;
						6'd9:	LCD_data = 8'b00110010;
						6'd10:	LCD_data = 8'b00110010;
						default:LCD_data=8'b00100000;
					endcase
				end
				else
				begin
					need=6'd16;
					case(which)
						6'd2:	LCD_data = 8'b01001110;	//N
						6'd3:	LCD_data = 8'b01101111;	//o
						6'd4:	LCD_data = 8'b00100000;	// 
						6'd5:	LCD_data = 8'b01110100;	//t
						6'd6:	LCD_data = 8'b01101000;	//h
						6'd7:	LCD_data = 8'b01101001;	//i
						6'd8:	LCD_data = 8'b01110011;	//s
						6'd9:	LCD_data = 8'b00100000;	//
						6'd10:	LCD_data = 8'b01101101;	//m	
						6'd11:	LCD_data = 8'b01100101;	//e
						6'd12:	LCD_data = 8'b01101101;	//m
						6'd13:	LCD_data = 8'b01100010;	//b
						6'd14:	LCD_data = 8'b01100101;	//e
						6'd15:	LCD_data = 8'b01110010;	//r
						default:LCD_data=8'b00100000;
					endcase
				end
			end
			else if(sw==3'b001)
			begin
				lcd_rs=1'b1; //rs 設為data
				need=6'd10;
				case(which)
					6'd2:	LCD_data=8'b01001000;//H  
					6'd3:	LCD_data=8'b01000101;//E
					6'd4:	LCD_data=8'b01001100;//L
					6'd5:	LCD_data=8'b01001100;//L
					6'd6:	LCD_data=8'b01001111;//O
					6'd7:	LCD_data=8'b00100000;
					6'd8:	LCD_data=8'b01000100;//D
					6'd9:	LCD_data=8'b01010011;//S
					6'd10:	LCD_data=8'b01001100;//L 
					default:LCD_data=8'b00100000;
				endcase
				
			end
			else if(sw==3'b010)//LSD HELLO @line 2
			begin
				lcd_rs=1'b1;
				need=6'd11;
				case(which)
					6'd2:	begin
							lcd_rs=1'b0;
							LCD_data=8'b10101000;//游標換行
							end
					6'd3:	LCD_data=8'b01001100;
					6'd4:	LCD_data=8'b01010011;
					6'd5:	LCD_data=8'b01000100;
					6'd6:	LCD_data=8'b00100000;
					6'd7:	LCD_data=8'b01001111;
					6'd8:	LCD_data=8'b01001100;
					6'd9:	LCD_data=8'b01001100;
					6'd10:	LCD_data=8'b01000101;
					6'd11:	LCD_data=8'b01001000;
					default:LCD_data=8'b00100000;
				endcase
			end
			else if(sw==3'b011)//clear
			begin
			need=6'd2;
			lcd_rs=1'b0;
			LCD_data=8'b00000001;
			end
			else if(sw==3'b100)//all dots
			begin
				need=6'd36;
				if(which==6'd18)
					begin
					lcd_rs=1'b0;
					LCD_data=8'b10101000;//游標換行
					end
				else 
				begin
				lcd_rs=1'b1;
				LCD_data=8'b11111111;
				end
			end
			else if(sw==3'b101)//left->right in line 1
			begin
				need=6'd16;
				lcd_rs=1'b1;
				LCD_data=8'b00100000;
				tmptime=28'd49999999;
			end
			else if(sw==3'b110)//right->left in line 2
			begin
				need=6'd17;
				if(which==6'd2) 
				begin
					lcd_rs=1'b0;
					LCD_data=8'b1001111;//游標換行
				end
				else
				begin
					lcd_rs=1'b0;
					LCD_data=8'b00010000;
					tmptime=28'd49999999;
				end
			end
			
			else if(sw==3'b111)//counter 0x
			begin
				need=6'd50;
				case (which)
					6'd2: LCD_data=8'b10101110;//move to 46
					6'd3: //write 0
					begin 
						lcd_rs=1'b1;
						LCD_data=8'b00110000;//0
					end
					6'd4:	LCD_data=8'b01111000;//x
					6'd5:begin
					lcd_rs=1'b1;
					case (number/10'd16)
						0: LCD_data=8'b00110000;//0
						1: LCD_data=8'b00110001;
						2: LCD_data=8'b00110010;
						3: LCD_data=8'b00110011;
						4: LCD_data=8'b00110100;
						5: LCD_data=8'b00110101;
						6: LCD_data=8'b00110110;
						7: LCD_data=8'b00110111;
						8: LCD_data=8'b00111000;
						9: LCD_data=8'b00111001;
						10: LCD_data=8'b01000001;//A
						11: LCD_data=8'b01000010;//B
						12: LCD_data=8'b01000011;//C
						13: LCD_data=8'b01000100;//D
						14: LCD_data=8'b01000101;//E
						15: LCD_data=8'b01000110;//F
						default: LCD_data=8'b01000111;//GG
					endcase
					end
					6'd6:begin
					lcd_rs=1'b1;
					case(number%10'd16)
						0: LCD_data=8'b00110000;//0
						1: LCD_data=8'b00110001;
						2: LCD_data=8'b00110010;
						3: LCD_data=8'b00110011;
						4: LCD_data=8'b00110100;
						5: LCD_data=8'b00110101;
						6: LCD_data=8'b00110110;
						7: LCD_data=8'b00110111;
						8: LCD_data=8'b00111000;
						9: LCD_data=8'b00111001;
						10: LCD_data=8'b01000001;//A
						11: LCD_data=8'b01000010;//B
						12: LCD_data=8'b01000011;//C
						13: LCD_data=8'b01000100;//D
						14: LCD_data=8'b01000101;//E
						15: LCD_data=8'b01000110;//F
						default: LCD_data=8'b01000111;//GG
					endcase
					end
					6'd7:
					begin//move back to 48
						lcd_rs=1'b0;
						LCD_data=8'b10110000;
						//tmptime=28'd10000000;
						tmptime=28'd25000000;
					end
					6'd8:
					begin
						which=6'd5;
						if(number==10'd255)
							howcount=1'b1;//往下
						else if(number==10'd0)
							howcount=1'b0;
						if(howcount)
							number=number-10'd1;
						else 
							number=number+10'd1;
					end
				endcase
			end
		end
		///以上是八個sw的情況
		if(counter>=tmptime+28'd80000) //  等到40000以上 拉低enable,counter 歸零,which+1 完成此筆swled亮第三顆
		begin 
		lcd_en=1'b0;   
		counter=28'd0;
		which=which+6'd1;
		led3=1'b1;
		staterr2=sw2;
		staterr=sw;
		end
		else if(counter>=tmptime+28'd40000)// 等到20000以上  拉高enable
		begin
		lcd_en=1'b1;
		end

	end
	counter=counter+28'd1;  //時間永遠加一
	end
*/
endmodule
