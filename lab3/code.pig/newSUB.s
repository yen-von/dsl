			In1 0						(2)
			In2 0
			In3 3
			In4		0
			In5		0
			In6		0
			In7		0
			In18	0
			In19	0
			In10	0
			In11	0
			In12	0
			In13	0
			In14    1
			In15    1
			B1   0 						(17)
			B2   0
			B3   3
			B4   0
			B5   0  
			B6   0
			B7   0
			B8   0
			B9   0
			B10  0
			B11  0
			B12  0
			B13  0
			B14  1
			B15  1
			C1   0 						(32)
			C2   0
			C3   2
			C4   0
			C5   0  
			C6   0
			C7   0
			C8   0
			C9   0
			C10  0
			C11  0
			C12  0
			C13  0
			C14  3
			C15  1
			SubTotalLen 0				(47)
			SubAFloat   0
			SubTmp1    0				
			SubTmp2	   0
			SubLen1	   0
			SubLen2    0
			SubFloat1  0
			SubFloat2  0
			SubVar1 [36]				(55)
			SubVar2 [36]				(91)
			SubAns  [36]				(125)	
			Lv2regT0	0
			Lv2regT1	0
			Lv2regT2	0
			Lv2regT3	0
			Lv2regT4	0
			Lv2regT5	0
			Lv2regT6	0
			Lv2regT7	0
			Load R4, #In1
			Load R0, #B1
			Load R1, #C1
			Call SubFunc
			Jump PEND
SubFunc 	Store mem[Lv2regT0], R0
			Store mem[Lv2regT1], R1
			Store mem[Lv2regT2], R2
			Store mem[Lv2regT3], R3
			Store mem[Lv2regT4], R4
			Store mem[Lv2regT5], R5
			Store mem[Lv2regT6], R6
			Store mem[Lv2regT7], R7
			Load R3, #SubTmp1
			Load R4, #114
			Load R5, #1
SubInit		Sub R4, R5
			Jump SubStart
			Load R6, #0
			Store mem[R3], R6
			Add R3, R5
			Jump SubInit
SubStart	Load R5, #1
			Add R0, R5
			Load R4, mem[R0]
			Store mem[SubFloat1], R4
			Add R0, R5
			Load R2, mem[R0]
			Store mem[SubLen1], R2
			Load R5, #13
			Add R0, R5
			Sub R0, R2
			Load R6, #SubVar1
			Add R6, R5
			Load R5, #1
			Sub R6, R5
			Sub R6, R4
SubCpLp1	Sub R2, R5
			Jump.C SubCp1Done
			Load R4, mem[R0]
			Store mem[R6], R4
			Add R0, R5
			Add R6, R5
			Jump SubCpLp1
SubCp1Done	Add R1, R5
			Load R4, mem[R1]
			Store mem[SubFloat2], R4
			Add R1, R5
			Load R3, mem[R1]
			Store mem[SubLen2], R3
			Load R5, #13
			Add R1, R5
			Sub R1, R3
			Load R6, #SubVar2
			Add R6, R5
			Load R5, #1
			Sub R6, R5
			Sub R6, R4
SubCpLp2	Sub R3, R5
			Jump.C SubSetLeft
			Load R4, mem[R1]
			Store mem[R6], R4
			Add R1, R5
			Add R6, R5
			Jump SubCpLp2
SubSetLeft	Load R0, mem[SubFloat1]
			Load R1, mem[SubFloat2]
			Sub R0, R1
			Jump.C	SubF2L
			Add R0, R1
			Mov R2, R0
			Jump SubSetRight
SubF2L		Mov R2, R1
SubSetRight	Load R0, mem[SubLen1]
			Load R1, mem[SubLen2]
			Load R3, mem[SubFloat1]
			Load R4, mem[SubFloat2]
			Sub R0, R3
			Sub R1, R4
			Sub R0, R1
			Jump.C SubL2L
			Add R0, R1
			Mov R3, R0
			Jump SubComp
SubL2L		Mov R3, R1
SubComp		Load R0, mem[SubLen1]
			Load R1, mem[SubLen2]
			Load R6, mem[SubFloat1]
			Load R7, mem[SubFloat2]
			Sub R0, R6
			Sub R1, R7
			Sub R0, R1 
			Jump.C SubSwitch
			Load R0, mem[SubLen1]
			Sub R0, R6
			Sub R1, R0
			Jump.NC SubDigCom
			Load R0, #SubVar1
			Load R1, #SubVar2
			Jump SubMain
SubDigCom	Mov R4, R0
			Load R0, #SubVar1
			Load R1, #SubVar2
			Load R5, #12
			Add R0, R5
			Add R0, R4
			Add R1, R5
			Add R1, R4
			Add R4, R2
			Load R5, #1
			Sub R0, R5
			Sub R1, R5
SubDLoop	Sub R4, R5
			Jump.C  SubDLpDone
			Load R6, mem[R0]
			Load R7, mem[R1]
			Sub R6, R7
			Jump.C SubSwitch
			Load R6, mem[R0]
			Sub R7, R6
			Jump.C	SubDLpDone
			Sub R0, R5
			Sub R1, R5
			Jump SubDLoop
SubSwitch 	Load R0, #SubVar2
			Load R1, #SubVar1
			Load R6, mem[Lv2regT4]
			Load R5, #1
			Store mem[R6], R5
			Jump SubMain
SubDLpDone	Load R0, #SubVar1
			Load R1, #SubVar2
SubMain		Add R3, R2
			Store mem[SubTotalLen], R3
			Store mem[SubAFloat], R2
			Load R4, #SubAns
			Load R5, #12
			Add R0, R5
			Add R1, R5
			Add R4, R5
			Sub R0, R2
			Sub R1, R2
			Sub R4, R2
			Load R5, #1
			Load R2, #0
SubMLoop	Sub R3, R5
			Jump.C	SubMLpDone
			Load R6, mem[R0]
			Load R7, mem[R1]
			Add R0, R5
			Add R1, R5
			Sub R6, R7
			Jump.C SubDigNeg
			Jump SubStAns
SubDigNeg   Load R5, #1
			Sub R0, R5
			Load R6, mem[R0]
			Add R0, R5
			Load R5, #10
			Add R6, R5
			Sub R6, R7
			Load R5, #1
			Load R7, mem[R0]
			Sub R7, R5
			Jump.NC SubDDD
			Load R5, #10
			Load R7, mem[R1]
			Add R7, R5
			Store mem[R1], R7
			Load R5, #1
			Load R7, #9
SubDDD		Store mem[R0], R7
SubStAns	Load R5, #1
			Sub R0, R5
			Sub R1, R5
			Store mem[R0], R2
			Store mem[R1], R2
			Store mem[R4], R6
			Add R0, R5
			Add R1, R5
			Add R4, R5
			Jump SubMLoop
SubMLpDone	Load R4, mem[Lv2regT4]
			Load R2, mem[SubAFloat]
			Load R3, mem[SubTotalLen]
			Load R0, #SubAns
			Load R5, #12
			Add R0, R5
			Load R5, #1
			Add R0, R3
			Sub R0, R2
			Sub R0, R5
			Load R7, mem[R0]
			Sub R7, R5
			Jump.NC SubCp
			Sub R3, R5
			Load R5, #14
			Add R4, R5
			Sub R4, R3
			Load R5, #1
			Load R6, #0
			Store mem[R4], R6
			Load R4, mem[Lv2regT4]
SubCp		Load R0, #SubAns
			Add R4, R5
			Store mem[R4], R2
			Add R4, R5
			Store mem[R4], R3
			Load R5, #12
			Add R4, R5
			Add R0, R5
			Sub R0, R2
			Add R0, R3
			Load R5, #1
			Sub R0, R5
			Load R7, #0
SubOutLoop	Sub R3, R5
			Jump.C SubDone
			Load R6, mem[R0]
			Store mem[R0], R7
			Store mem[R4], R6
			Sub R0, R5
			Sub R4, R5
			Jump SubOutLoop
SubDone		Load R0, mem[Lv2regT0]
			Load R1, mem[Lv2regT1]
			Load R2, mem[Lv2regT2]
			Load R3, mem[Lv2regT3]
			Load R4, mem[Lv2regT4]
			Load R5, mem[Lv2regT5]
			Load R6, mem[Lv2regT6]
			Load R7, mem[Lv2regT7]
			Return
PEND 		Mov R0, R0