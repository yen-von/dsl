			In1 0						(2)
			In2 0
			In3 1
			In4		0
			In5		0
			In6		0
			In7		0
			In18	0
			In19	0
			In10	0
			In11	0
			In12	0
			In13	0
			In14    0
			In15    3
			B1   0 						(17)
			B2   0
			B3   1
			B4   0
			B5   0  
			B6   0
			B7   0
			B8   0
			B9   0
			B10  0
			B11  0
			B12  0
			B13  0
			B14  0
			B15  3
			C1   0 						(32)
			C2   2
			C3   3
			C4   0
			C5   0  
			C6   0
			C7   0
			C8   0
			C9   0
			C10  0
			C11  0
			C12  0
			C13  5
			C14  0
			C15  0
			AddTotalLen 0				(47)
			AddAFloat   0
			AddTmp1    0				
			AddTmp2	   0
			AddLen1	   0
			AddLen2    0
			AddFloat1  0
			AddFloat2  0
			AddVar1 [36]				(55)
			AddVar2 [36]				(91)
			AddAns  [36]				(127)	
			Lv2regT0	0
			Lv2regT1	0
			Lv2regT2	0
			Lv2regT3	0
			Lv2regT4	0
			Lv2regT5	0
			Lv2regT6	0
			Lv2regT7	0
			Lv1regT0	0
			Lv1regT1	0
			Lv1regT2	0
			Lv1regT3	0
			Lv1regT4	0
			Lv1regT5	0
			Lv1regT6	0
			Lv1regT7	0
			MulFloat1	0			(179)
			MulFloat2	0
			MulPos1		0
			MulPos2		0
			MulLen1		0
			MulLen2		0
			MulTemp	   [15]			(185)
			MulAns     [15]			(200)
			MulOut	   [25]			(215)
			MulOutLen	0			(R0 as in1, R1 as in2, R4 as out)
			Load R4, #In1
			Load R0, #B1
			Load R1, #C1
			Call MulFunc
			Jump PEND
MulFunc		Store mem[Lv1regT0], R0
			Store mem[Lv1regT1], R1
			Store mem[Lv1regT2], R2
			Store mem[Lv1regT3], R3
			Store mem[Lv1regT4], R4
			Store mem[Lv1regT5], R5
			Store mem[Lv1regT6], R6
			Store mem[Lv1regT7], R7
			Load R3, #MulTemp
			Load R4, #55
			Load R5, #1
MulInit		Sub R4, R5
			Jump.C MulStart
			Load R6, #0
			Store mem[R3], R6
			Add R3, R5
			Jump MulInit
MulStart	Load R6, #MulAns
			Load R7, #0
			Load R5, #1
			Store mem[R6], R7
			Add R6, R5
			Store mem[R6], R7
			Add R6, R5
			Store mem[R6], R7
			Load R6, mem[R0]
			Store mem[MulPos1], R6
			Add R0, R5
			Load R5, #0
			Load R3, mem[R0]
			Store mem[MulFloat1], R3
			Store mem[R0], R5
			Load R5, #1
			Sub R0, R5
			Load R6, mem[R1]
			Store mem[MulPos2], R6
			Add R1, R5
			Load R3, mem[R1]
			Store mem[MulFloat2], R3
			Load R5, #0
			Store mem[R1], R5
			Load R5, #1
			Add R1, R5
			Load R3, mem[R1]			(R3 len of in2)
			Store mem[MulLen2], R3
			Load R5, #13
			Add R1, R5
			Sub R1, R3					(R1 ready)
			Load R4, #MulAns
			Load R5, #1
MulLp1		Sub R3, R5
			Load R4, #MulAns
			Jump.C	MulDoOut
			Load R5, #1
			Load R2, mem[R1]
			Add R1, R5
MulLp2		Sub R2, R5
			Jump.C MulLp2Done
			Mov R7, R1
			Mov R1, R4
			Call AddFunc
			Mov R1, R7
			Jump MulLp2
MulLp2Done  Add R4, R5
			Add R4, R5
			Load R7, mem[R4]
			Mov R6, R7
			Load R5, #13
			Sub R7, R5
			Jump.NC MulOverFlow
			Add R4, R5
			Load R5, #1
			Sub R6, R5
			Jump.C MulAnsCl
			Jump MulAnsRe
MulAnsCl	Mov R6, R5
			Jump MulBreak2
MulAnsRe	Add R6, R5
MulBreak2	Sub R4, R6
			Load R7, mem[R4]
			Load R5, #0
			Store mem[R4], R5
			Load R5, mem[MulOutLen]
			Load R6, #MulOut
			Add R6, R5
			Store mem[R6], R7
			Load R6, #1
			Add R5, R6
			Store mem[MulOutLen], R5
			Load R5, #1
			Load R4, #MulAns
			Add R4, R5
			Add R4, R5
			Load R6, mem[R4]
			Sub R6, R5
			Store mem[R4], R6
			Jump MulLp1
MulOverFlow Load R7, #12
			Store mem[R4], R7
			Jump MulLp1
MulDoOut	Load R4, #MulTemp
			Load R0, #MulAns
			Load R1, #MulOut
			Load R3, mem[MulOutLen]
			Load R5, #1
			Load R6, mem[MulPos1]
			Load R7, mem[MulPos2]
			Xor R6, R7
			Store mem[R4], R6
			Add R4, R5
			Load R6, mem[MulFloat1]
			Load R7, mem[MulFloat2]
			Add R6, R7
			Store mem[R4], R6
			Add R4, R5
			Add R0, R5
			Add R0, R5
			Load R2, mem[R0]
MulOutStart Add R0, R5
			Mov R6, R2
			Add R6, R3
			Load R5, #12
			Sub R6, R5
			Jump.NC MulOOR
			Mov R6, R2
			Add R6, R3
			Store mem[R4], R6				(Move MulAns and MulOut to realout)
MulBreak	Load R5, #1
			Add R4, R5
			Load R5, #12
			Add R0, R5
			Add R4, R5
			Sub R4, R6
			Sub R0, R2
			Load R5, #1
			Jump MulMvOut
MulOOR		Add R1, R6
			Sub R3, R6
			Load R6, #12
			Store mem[R4], R6
			Jump MulBreak
MulMvOut	Sub R3, R5
			Jump.C MulMvAns
			Load R6, mem[R1]
			Store mem[R4], R6
			Add R1, R5
			Add R4, R5
			Jump MulMvOut
MulMvAns	Sub R2, R5
			Jump.C MulMvTemp
			Load R6, mem[R0]
			Store mem[R4], R6
			Add R0, R5
			Add R4, R5
			Jump MulMvAns
MulMvTemp	Load R0, #MulTemp
			Load R1, mem[R0]
			Add R0, R5
			Load R2, mem[R0]
			Add R0, R5
			Load R3, mem[R0]
			Mov R4, R3
			Sub R4, R2
			Sub R4, R5
			Load R5, #12
			Add R0, R5
			Load R6, #0
			Load R5, #1
MulChkTemp	Sub R4, R5
			Jump.C MulChkTDone
			Load R7, mem[R0]
			Sub R7, R5
			Jump.NC MulChkTDone
			Sub R0, R5
			Add R6, R5
			Jump MulChkTemp
MulChkTDone	Sub R3, R6   (R0 is the ready place of temp)
			Load R4, mem[Lv1regT4]
			Store mem[R4], R1
			Add R4, R5
			Store mem[R4], R2
			Add R4, R5
			Store mem[R4], R3
			Load R5, #12
			Add R4, R5
			Load R5, #1
MulMvROut	Sub R3, R5
			Jump.C MulMvDone
			Load R7, mem[R0]
			Store mem[R4], R7
			Sub R0, R5
			Sub R4, R5
			Jump MulMvROut
MulMvDone	Load R0, mem[Lv1regT0]
			Load R1, mem[Lv1regT1]
			Load R5, #1
			Add R1, R5
			Load R6, mem[MulFloat2]
			Store mem[R1], R6
			Sub R1, R5
			Load R2, mem[Lv1regT2]
			Load R3, mem[Lv1regT3]
			Load R4, mem[Lv1regT4]
			Load R5, mem[Lv1regT5]
			Load R6, mem[Lv1regT6]
			Load R7, mem[Lv1regT7]
			Return
AddFunc 	Store mem[Lv2regT0], R0
			Store mem[Lv2regT1], R1
			Store mem[Lv2regT2], R2
			Store mem[Lv2regT3], R3
			Store mem[Lv2regT4], R4
			Store mem[Lv2regT5], R5
			Store mem[Lv2regT6], R6
			Store mem[Lv2regT7], R7
			Load R3, #AddTmp1
			Load R4, #114
			Load R5, #1
AddInit		Sub R4, R5
			Jump.C	AddStart
			Load R6, #0
			Store mem[R3], R6
			Add R3, R5
			Jump AddInit
AddStart	Load R5, #1
			Add R0, R5
			Load R4, mem[R0]
			Store mem[AddFloat1], R4
			Add R0, R5
			Load R2, mem[R0]
			Store mem[AddLen1], R2
			Load R5, #13
			Add R0, R5
			Sub R0, R2
			Load R6, #AddVar1
			Add R6, R5
			Load R5, #1
			Sub R6, R5
			Sub R6, R4
AddCpLp1	Sub R2, R5
			Jump.C AddCp1Done
			Load R4, mem[R0]
			Store mem[R6], R4
			Add R0, R5
			Add R6, R5
			Jump AddCpLp1
AddCp1Done	Add R1, R5
			Load R4, mem[R1]
			Store mem[AddFloat2], R4
			Add R1, R5
			Load R3, mem[R1]
			Store mem[AddLen2], R3
			Load R5, #13
			Add R1, R5
			Sub R1, R3
			Load R6, #AddVar2
			Add R6, R5
			Load R5, #1
			Sub R6, R5
			Sub R6, R4
AddCpLp2	Sub R3, R5
			Jump.C AddSetLeft
			Load R4, mem[R1]
			Store mem[R6], R4
			Add R1, R5
			Add R6, R5
			Jump AddCpLp2
AddSetLeft	Load R0, mem[AddFloat1]
			Load R1, mem[AddFloat2]
			Sub R0, R1
			Jump.C	AddF2L
			Add R0, R1
			Mov R2, R0
			Jump AddSetRight
AddF2L		Mov R2, R1
AddSetRight	Load R0, mem[AddLen1]
			Load R1, mem[AddLen2]
			Load R3, mem[AddFloat1]
			Load R4, mem[AddFloat2]
			Sub R0, R3
			Sub R1, R4
			Sub R0, R1
			Jump.C AddL2L
			Add R0, R1
			Mov R3, R0
			Jump AddMain
AddL2L		Mov R3, R1
AddMain		Add R3, R2
			Store mem[AddTotalLen], R3
			Store mem[AddAFloat], R2
			Load R0, #AddVar1
			Load R1, #AddVar2
			Load R4, #AddAns
			Load R5, #12
			Add R0, R5
			Add R1, R5
			Add R4, R5
			Sub R0, R2
			Sub R1, R2
			Sub R4, R2
			Load R5, #1
AddMLoop	Sub R3, R5
			Jump.C AddMLpDone
			Load R6, mem[R0]
			Load R7, mem[R1]
			Add R6, R7
			Add R0, R5
			Add R1, R5
			Load R5, #10
			Sub R6, R5
			Jump.C AddNC
			Load R5, #1
			Load R7, mem[R0]
			Add R7, R5
			Store mem[R0], R7
			Sub R3, R5
			Jump.NC AddNoBigC
			Add R3, R5
AddNoBigC	Add R3, R5
			Jump AddStAns
AddNC		Load R5, #1
			Sub R0, R5
			Load R6, mem[R0]
			Add R6, R7
			Add R0, R5
AddStAns	Load R5, #1
			Store mem[R4], R6
			Add R4, R5
			Jump AddMLoop
AddMLpDone	Load R4, mem[Lv2regT4]
			Load R2, mem[AddAFloat]
			Load R3, mem[AddTotalLen]
			Load R0, #AddAns
			Load R5, #12
			Add R0, R5
			Sub R0, R2
			Add R0, R3
			Load R6, mem[R0]
			Load R5, #1
			Sub R6, R5
			Jump.NC AddCin
			Sub R0, R5
			Jump AddOut
AddCin		Add R3, R5
AddOut		Add R4, R5
			Store mem[R4], R2
			Add R4, R5
			Store mem[R4], R3
			Load R5, #12
			Add R4, R5
			Load R5, #1
AddOutLp	Sub R3, R5
			Jump.C	AddDone
			Load R6, mem[R0]
			Store mem[R4], R6
			Sub R0, R5
			Sub R4, R5
			Jump AddOutLp
AddDone		Load R0, mem[Lv2regT0]
			Load R1, mem[Lv2regT1]
			Load R2, mem[Lv2regT2]
			Load R3, mem[Lv2regT3]
			Load R4, mem[Lv2regT4]
			Load R5, mem[Lv2regT5]
			Load R6, mem[Lv2regT6]
			Load R7, mem[Lv2regT7]
			Return
PEND		Mov R0, R0