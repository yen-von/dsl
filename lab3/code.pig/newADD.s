			In1 0						(2)
			In2 0
			In3 3
			In4		0
			In5		0
			In6		0
			In7		0
			In18	0
			In19	0
			In10	0
			In11	0
			In12	0
			In13	3
			In14    2
			In15    1
			B1   0 						(17)
			B2   0
			B3   4
			B4   0
			B5   0  
			B6   0
			B7   0
			B8   0
			B9   0
			B10  0
			B11  0
			B12  4
			B13  0
			B14  2
			B15  6
			C1   0 						(32)
			C2   0
			C3   3
			C4   0
			C5   0  
			C6   0
			C7   0
			C8   0
			C9   0
			C10  0
			C11  0
			C12  0
			C13  1
			C14  2
			C15  3
			AddTotalLen 0				(47)
			AddAFloat   0
			AddTmp1    0				
			AddTmp2	   0
			AddLen1	   0
			AddLen2    0
			AddFloat1  0
			AddFloat2  0
			AddVar1 [36]				(55)
			AddVar2 [36]				(91)
			AddAns  [36]				(125)	
			Lv2regT0	0
			Lv2regT1	0
			Lv2regT2	0
			Lv2regT3	0
			Lv2regT4	0
			Lv2regT5	0
			Lv2regT6	0
			Lv2regT7	0
			Load R4, #In1		(R0 is the addr of In1, R1 is the addr of In2, R4 is the addr of Out)
			Load R0, #B1
			Load R1, #C1
AddFunc 	Store mem[Lv2regT0], R0
			Store mem[Lv2regT1], R1
			Store mem[Lv2regT2], R2
			Store mem[Lv2regT3], R3
			Store mem[Lv2regT4], R4
			Store mem[Lv2regT5], R5
			Store mem[Lv2regT6], R6
			Store mem[Lv2regT7], R7
			Load R3, #AddTmp1
			Load R4, #114
			Load R5, #1
AddInit		Sub R4, R5
			Jump.C	AddStart
			Load R6, #0
			Store mem[R3], R6
			Add R3, R5
			Jump AddInit
AddStart	Load R5, #1
			Add R0, R5
			Load R4, mem[R0]
			Store mem[AddFloat1], R4
			Add R0, R5
			Load R2, mem[R0]
			Store mem[AddLen1], R2
			Load R5, #13
			Add R0, R5
			Sub R0, R2
			Load R6, #AddVar1
			Add R6, R5
			Load R5, #1
			Sub R6, R5
			Sub R6, R4
AddCpLp1	Sub R2, R5
			Jump.C AddCp1Done
			Load R4, mem[R0]
			Store mem[R6], R4
			Add R0, R5
			Add R6, R5
			Jump AddCpLp1
AddCp1Done	Add R1, R5
			Load R4, mem[R1]
			Store mem[AddFloat2], R4
			Add R1, R5
			Load R3, mem[R1]
			Store mem[AddLen2], R3
			Load R5, #13
			Add R1, R5
			Sub R1, R3
			Load R6, #AddVar2
			Add R6, R5
			Load R5, #1
			Sub R6, R5
			Sub R6, R4
AddCpLp2	Sub R3, R5
			Jump.C AddSetLeft
			Load R4, mem[R1]
			Store mem[R6], R4
			Add R1, R5
			Add R6, R5
			Jump AddCpLp2
AddSetLeft	Load R0, mem[AddFloat1]
			Mov R0, R0
			Load R1, mem[AddFloat2]
			Sub R0, R1
			Jump.C	AddF2L
			Add R0, R1
			Mov R2, R0
			Jump AddSetRight
AddF2L		Mov R2, R1
AddSetRight	Load R0, mem[AddLen1]
			Load R1, mem[AddLen2]
			Load R3, mem[AddFloat1]
			Load R4, mem[AddFloat2]
			Sub R0, R3
			Sub R1, R4
			Sub R0, R1
			Jump.C AddL2L
			Add R0, R1
			Mov R3, R0
			Jump AddMain
AddL2L		Mov R3, R1
AddMain		Add R3, R2
			Store mem[AddTotalLen], R3
			Store mem[AddAFloat], R2
			Load R0, #AddVar1
			Load R1, #AddVar2
			Load R4, #AddAns
			Load R5, #12
			Add R0, R5
			Add R1, R5
			Add R4, R5
			Sub R0, R2
			Sub R1, R2
			Sub R4, R2
			Load R5, #1
AddMLoop	Sub R3, R5
			Jump.C AddMLpDone
			Load R6, mem[R0]
			Load R7, mem[R1]
			Add R6, R7
			Add R0, R5
			Add R1, R5
			Load R5, #10
			Sub R6, R5
			Jump.C AddNC
			Load R5, #1
			Load R7, mem[R0]
			Add R7, R5
			Store mem[R0], R7
			Mov R7, R3
			Sub R7, R5
			Jump.NC AddNoBigC
			Add R3, R5
AddNoBigC	Jump AddStAns
AddNC		Load R5, #1
			Sub R0, R5
			Load R6, mem[R0]
			Add R6, R7
			Add R0, R5
AddStAns	Load R5, #1
			Store mem[R4], R6
			Add R4, R5
			Jump AddMLoop
AddMLpDone	Load R4, mem[Lv2regT4]
			Load R2, mem[AddAFloat]
			Load R3, mem[AddTotalLen]
			Jump PEND
			Load R0, #AddAns
			Load R5, #12
			Add R0, R5
			Sub R0, R2
			Add R0, R3
			Load R6, mem[R0]
			Load R5, #1
			Sub R6, R5
			Jump.NC AddCin
			Sub R0, R5
			Jump AddOut
AddCin		Add R3, R5
AddOut		Add R4, R5
			Store mem[R4], R2
			Add R4, R5
			Store mem[R4], R3
			Load R5, #12
			Add R4, R5
			Load R5, #1
AddOutLp	Sub R3, R5
			Jump.C	AddDone
			Load R6, mem[R0]
			Store mem[R4], R6
			Sub R0, R5
			Sub R4, R5
			Jump AddOutLp
AddDone		Mov R0, R0
PEND		Mov R0, R0