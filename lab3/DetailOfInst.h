#instruction set:                          OP1               OP2              OP3           OP4          OP5

DECODE :
	1. PC->MA
	2. Memory Read
	3. Memory Data in MDR
	4. MDR->IR
	5. IR->decoder
PC inc:
	1. PC->X
	2. X+1->W(ALU)
	3. W->PC
#01: Load Rx, #imm                         00010000-00000xxx dddddddddddddddd// Load constant number to Rx
	Step:
		1. DECDOE
		2. PC inc
		3. PC->MA
		4. Memory Read
		5. Memory Data in MDR
		6. MDR->Rx
		7. PC inc

#02: Load Rx, mem[Ry]                      00010001-00xxxyyy //xxx represents the number of regx, yyy represents the number of regy
	Step:
		1. DECODE
		2. Ry->MA
		3. Memory Read
		4. Memory Data in MDR
		5. MDR->Rx
		6. PC inc
		
#03: Load Rx, mem[addr]                    00010010-00000xxx addr
	Step:
		1. DECODE
		2. PC inc
		3. PC->MA
		4. Memory Read
		5. Memory Data in MDR
		6. MDR->MA
		7. Memory Read
		8. Memory Data in MDR
		9. MDR->Rx
		
#04: Store mem[addr], Rx                   00100000-00000xxx  addr
	Step:
		1. DEODE
		2. PC inc
		3. PC->MA
		4. Memory Read
		5. Memor Data in MDR
		6. MDR->MA
		7. Rx->MDR
		8. memory write, PC inc
		
#05: Move Rx, Ry                           00110000-00xxxyyy
	Step:
		1. DECODE
		2. PC inc
		3. Rx<-Ry
#06: Swap Rx, Ry                           00110001-00xxxyyy
	Step:
		1. DECODE
		2. PC inc
		3. Rx->temp1
		4. temp1->Ry
		5. Ry->Rx
#07: Push Rx                               01000000-00000xxx	//Store it in temp2
	Step:
		1. DECODE
		2. PC inc
		3. Rx->temp2
#08: Add Rx, Ry                            01010000-00xxxyyy	// Result in R1
	Step:
		1. DECODE
		2. PC inc
		3. Rx->X
		4. Ry->Y
		5. ALU do W = X + Y
		6. W->R1
#09: Mul Rx, Ry                            01010001-00xxxyyy
#10: Pop Rx                                01000001-00000xxx
	Step:
		1. DECODE
		2. PC inc
		3. temp2->Rx
#11: Sub Rx, Ry                            01010010-00xxxyyy	//Result in R1
	Step:
		1. DECODE
		2. PC inc
		3. Rx->X
		4. Ry->Y
		5. ALU do W = X - Y
		6. W->R1
#12: Div Rx, Ry                            01010011-00xxxyyy
#13: And Rx, Ry                            01010100-00xxxyyy	//Result in R1
	Step:
		1. DECODE
		2. PC inc
		3. Rx->X
		4. Ry->Y
		5. ALU do W = X & Y
		6. W->R1
#14: Xor Rx, Ry                            01010101-00xxxyyy	//Result in R1
	Step:
		1. DECODE
		2. PC inc
		3. Rx->X
		4. Ry->Y
		5. ALU do W = X^Y
		6. W->R1
#15: Or Rx, Ry                         01010110-00xxxyyy
	Step:
		1. DECODE
		2. PC inc
		3. Rx->X
		4. Ry->Y
		5. ALU do W = X | Y
		6. W->R1
#16: Jump Loc(a label in asm)              01110000-........ addr(16bit)
	Step:
		1. DECODE
		2. PC inc
		3. PC->MA
		4. Memory Read
		5. Memory Data in MDR
		6. MDR->PC
#17: Jump.C Loc(a label in asm)            01110001-........ addr
	Step:
		1. DECODE
		2. PC inc
		3. PC->X
		4. C->Y
		5. ALU do W = X+!C
		6. W->PC
#18: Jump.NC Loc(a label in asm)           01110010-........ addr
	Step:
		1. DECODE
		2. PC inc
		3. PC->X
		4. C->Y
		5. ALU do W = X+C
		6. W->PC	
#19: Clear.C                               01110011-........
	Step:
		1. DECODE
		2. PC inc
		3. C = 0	//using verilog
#20: Set.C                                 10110100-........
	Step:
		1. DECODE
		2. PC inc
		3. C = 1
#21: Test.biti Rx (The purpose of this instruction is currently unknown!)
	Step:
		1. DECODE
		2. PC inc
		3. C->X
		4. Rx->Y
		5. ALU do W = X & Y
		6. W->R1