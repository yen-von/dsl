#instruction set:                          OP1               OP2              OP3           OP4          OP5
#01: Load Rx, #imm                         00010000-00000xxx dddddddddddddddd// Load constant number to Rx
#02: Load Rx, mem[Ry]  					   00010001-00xxxyyy //xxx represents the number of regx, yyy represents the number of regy
#03: Load Rx, mem[addr]                    00010010-........ LAST BYTE         THIRD BYTE     SECOND BYTE   FIRST BYTE
#04: Store mem[addr], Rx				   00100000-00000xxx LAST BYTE         THIRD BYTE     SECOND BYTE   FIRST BYTE
#05: Move Rx, Ry	                       00110000-00xxxyyy
#06: Swap Rx, Ry                           00110001-00xxxyyy
#07: Push Rx                               01000000-00000xxx
#08: Add Rx, Ry                            01010000-00xxxyyy
#09: Mul Rx, Ry							   01010001-00xxxyyy
#10: Pop Rx                                01000001-00000xxx
#11: Sub Rx, Ry							   01010010-00xxxyyy
#12: Div Rx, Ry                            01010011-00xxxyyy
#13: And Rx, Ry                            01010100-00xxxyyy
#14: Xor Rx, Ry                            01010101-00xxxyyy
#15: Or Rc, Rx, Ry                         01010110-00xxxyyy
#16: Jump Loc(a label in asm)              01110000-........ LAST BYTE         THIRD BYTE     SECOND BYTE   FIRST BYTE
#17: Jump.C Loc(a label in asm)            01110001-........ LAST BYTE         THIRD BYTE     SECOND BYTE   FIRST BYTE
#18: Jump.NC Loc(a label in asm)           01110010-........ LAST BYTE         THIRD BYTE     SECOND BYTE   FIRST BYTE
#19: Clear.C                               01110011-........
#20: Set.C                                  -........
#21: Test.biti Rx (The purpose of this instruction is currently unknown!)
