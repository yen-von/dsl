"""GUI for a CPU Simulator."""
#from Tkinter import *
from Tkinter import Label, Button, Frame, Tk, NW, SW, SE, W, N, E, S, StringVar, Entry, NE, Radiobutton, Scrollbar, Text, END
from Instructions import Instructions, Hardware
class GUIDemo(Frame):
    def __init__(self, master=None):
        """initial the window"""
        Frame.__init__(self, master)
        self.grid()
        self.dev = Hardware(mem_size=65536)   
        self.displayText = Label(self, text="----Welcome to the SIMP-16 "
                                           "simulator !----",fg="red")
        self.displayText.grid(row=0, column=0, columnspan=7)
        self.MText = Label(self, text="Memory")
        self.MText.grid(row=1, column=0)
        self.RText = Label(self, text="Register")
        self.RText.grid(row=1, column=1)
        self.RDText = Label(self, text="Record")
        self.RDText.grid(row=1, column=2)
        self.IText = Label(self, text="Input")
        self.IText.grid(row=1, column=4)
        
        #memory showbox
        rpos = 4
        cpos = 0
        self.membox = Label(self)
        self.membox.grid(row=rpos, column=cpos, sticky=NW)
            #up and down to control mem
        rpos = 5
        cpos = 0
        self.mem_offset = 0
        self.UP = Button(self, text=" UP ", fg="red", command=self.UpMethod)
        self.UP.grid(row=rpos, column=cpos+0, sticky=W)

        self.Down = Button(self, text="Down", fg="green", command=self.DownMethod)
        self.Down.grid(row=rpos, column=cpos+0, sticky=E)

        self.Set_offset = Button(self, text="Set Memory to:",
                                 command=self.Set_offsetMethod)
        self.Set_offset.grid(row=rpos, column=cpos+0, sticky=S)
        self.offset_num = StringVar()
        self.offset_numbox = Entry(self, width=10, textvariable=self.offset_num)
        self.offset_numbox.grid(row=rpos+1, column = cpos)
            #print way
        ways = [("binary", "b", NW),("hex", "h", N),("decimal", "d", NE)]
        self.printway = StringVar()
        self.printway.set("b")
        for text, mode, st in ways:
            b = Radiobutton(self, text=text, variable=self.printway, value=mode,
                            command=self.ShowMethod)
            b.grid(row=rpos+3, column=cpos, sticky=st)

        #
        
        #reg showbox
        rpos = 4
        cpos = 1
        self.regbox = Label(self)
        self.regbox.grid(row=rpos, column=cpos, sticky=NW)   
        #
        
        #input and output box
        rpos = 4
        cpos = 2
        
        self.donebox_scrollbar = Scrollbar(self)
        self.donebox_scrollbar.grid(row=rpos, column=1+cpos)
        self.donebox = Text(self, yscrollcommand=self.donebox_scrollbar.set,
                            width=50)
        self.donebox.grid(row=rpos, column=0+cpos)
        self.donebox_scrollbar.config(command=self.donebox.yview)
        self.Clear = Button(self, text="Clear", command=self.ClearMethod)
        self.Clear.grid(row=rpos, column=cpos, sticky=S, rowspan=2)

        
        self.instructbox = Text(self, width=30)
        self.instructbox.grid(row=rpos, column=2+cpos)
        self.inputcode=""
        self.h = 0
        self.e = 0
        self.lastins = ""
        self.PCText = Label(self, text="Program Counter: "+str(self.dev.pc))
        self.PCText.grid(row=rpos-2, column=2+cpos, rowspan=2, sticky=N)
        self.Step = Button(self, text="Step", fg="red", command=self.StepMethod)
        self.Step.grid(row=rpos, column=2+cpos, sticky=SW, rowspan=2)
        self.Run = Button(self, text="Run", fg="blue",
                          command=self.RunMethod, width=6)
        self.Run.grid(row=rpos, column=2+cpos, sticky=SE, rowspan=2)
        
        #
        self.ShowMethod()
        
    def ShowMethod(self):
        tmpstyle = self.printway.get()
        self.membox["text"] = (self.dev.show_mem(offset=self.mem_offset,
                               how=tmpstyle))
        self.regbox["text"] = (self.dev.show_reg(how=tmpstyle)+ "\n"
                               + self.dev.show_tmp(how=tmpstyle)+"\n"
                               + self.dev.show_alu(how=tmpstyle)+"\n"
                               + self.dev.show_mar(how=tmpstyle))
        self.PCText["text"] = "Program Counter: "+str(self.dev.pc)
    def DownMethod(self):
        self.displayText["text"] = "Memory display go up"
        if(self.mem_offset==self.dev.mem_size-20):
            self.displayText["text"] = "Can't go higher"
            return 
        self.mem_offset += 1
        self.ShowMethod()
        return
        
    def UpMethod(self):
        self.displayText["text"] = "Memory display go down"
        if(self.mem_offset==0):
            self.displayText["text"] = "Can't go lower"
            return 
        self.mem_offset -= 1
        self.ShowMethod()
        return 
        
    def Set_offsetMethod(self):
        if self.offset_num.get().isdigit():
            tmp = int(self.offset_num.get())
            if tmp <= 0:
                tmp = 0
            elif int(self.offset_num.get()) > self.dev.mem_size-20:
                tmp = self.dev.mem_size-20
            self.mem_offset = tmp
            self.displayText["text"] = "set memory to "+str(hex(tmp))
        
        else:
            self.displayText["text"] = "Can't set to : "+self.offset_num.get()
        self.ShowMethod()
        return
    def StepMethod(self):       
        self.displayText["text"] = "Do a step!"
        self.PCText["text"] = "Program Counter: "+str(self.dev.pc)
        if(self.dev.pc==-1):
            self.inputcode = (self.instructbox.get("0.0",'end-1c')).rstrip("\n").split("\n")
            tmp = 0
            for i in self.inputcode:
                self.dev.mem[tmp] = map(int, list(i))
                tmp += 1
            self.dev.pc = 0
            self.donebox.insert(END,"Load code in memory\n")
            self.donebox.yview(END)
            self.ShowMethod()
            return 
        if(self.dev.pc >= len(self.inputcode)):
            self.donebox.insert(END,"---\nprogramm end.\n")
            self.donebox.yview(END)
            self.dev.pc = len(self.inputcode)+1
            return 
        else:
            self.instructbox.delete(str(self.h+1)+".0", str(self.e+1)+".0")
            self.instructbox.insert(str(self.h+1)+".0", self.lastins)
            self.h = self.dev.pc
            tmpstr = ""
            self.lastins = ""
            self.donebox.insert(END, Instructions(self.inputcode, self.dev))
            self.e = self.dev.pc
            if(self.h < self.e):
                for i in range(self.h, self.e):
                    tmpstr += "-> "+self.inputcode[i]+"\n"
                    self.lastins += self.inputcode[i]+"\n"
            else:
                tmpstr += "-> "+self.inputcode[self.h]+"\n"
            self.instructbox.delete(str(self.h+1)+".0", str(self.e+1)+".0")
            self.instructbox.insert(str(self.h+1)+".0", tmpstr)
            
            self.donebox.see(END)
        self.ShowMethod()
        return 
    def RunMethod(self):
        self.displayText["text"] = "Run the whole programm!"
        self.StepMethod()
        while(self.dev.pc <= len(self.inputcode)):
            self.StepMethod()
        return 
    def ClearMethod(self):
        self.displayText["text"] = "Clear"
        self.donebox.delete("0.0", END)
        self.instructbox.delete("0.0", END)
        self.ShowMethod()
        self.dev = Hardware(mem_size=65536)
        self.mem_offset = 0
        self.ShowMethod()
        return 
 
if __name__ == '__main__':
    root = Tk()
    #root.geometry("755x550")
    root.title("SIMP-16 simulator")
    app = GUIDemo(master=root) 
    app.mainloop()
