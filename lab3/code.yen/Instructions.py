from __future__ import print_function

def Instructions(inputcode,dev):
    ins=inputcode[dev.pc]
    if ins.isdigit() is False :
        pc+=1
        return ["Instruction: \'"+ins+"\' is wrong\n"]   
    if cmp(ins,"")==0: #nothing
        ins_num=0
        ins_str="\n"
    elif cmp(ins[0:13],"0001000000000")==0:
        ins_str="---\nLoad data:"+inputcode[dev.pc+1]+" into reg["+str(int(ins[-3:],2))+"]\n"
        dev.reg[int(ins[-3:],2)]=map(int,list(inputcode[dev.pc+1]))
        dev.pc+=2
    elif cmp(ins[0:10],"0001000100")==0:  
        ins_num=2
        ins_str="---\nreg["+str(int(ins[-3:],2))+"]:"+"".join(map(str,dev.reg[int(ins[-3:],2)]))+" = "+int("".join(map(str,dev.reg[int(ins[-3:],2)])),2).__hex__()+"\nLoad mem["+int("".join(map(str,dev.reg[int(ins[-3:],2)])),2).__hex__()+"] in reg["+str(int(ins[-6:-3],2))+"]\n"
        dev.reg[int(ins[-6:-3],2)]=dev.mem[int("".join(map(str,dev.reg[int(ins[-3:],2)])),2)]
        dev.pc+=1
    elif cmp(ins[0:8],"00010010")==0:  
        ins_num=3
        ins_str="---\nMemory address:"+inputcode[dev.pc+1]+" = "+int(inputcode[dev.pc+1],2).__hex__()+"\nLoad mem["+int(inputcode[dev.pc+1],2).__hex__()+"] into reg["+str(int(ins[-3:],2))+"] \n"
        dev.reg[int(ins[-3:],2)]=dev.mem[int(inputcode[dev.pc+1],2)]
        dev.pc+=2
    elif cmp(ins[0:8],"00100000")==0:  
        ins_num=4
        ins_str="---\nMemory address:"+inputcode[dev.pc+1]+" = "+int(inputcode[dev.pc+1],2).__hex__()+"\nStore reg["+str(int(ins[-3:],2))+"]"+" into mem["+int(inputcode[dev.pc+1],2).__hex__()+"]\n"
        dev.mem[int(inputcode[dev.pc+1],2)]=dev.reg[int(ins[-3:],2)]
        dev.pc+=2
    elif cmp(ins[0:8],"00100001")==0:  
        ins_num=4.5
        ins_str="---\nreg["+str(int(ins[-3:],2))+"]= "+"".join(map(str,dev.reg[int(ins[-3:],2)]))+"\nStore reg["+str(int(ins[-6:-3],2))+"]"+" into mem[reg["+str(int(ins[-3:],2))+"]]\n"
        dev.mem[int("".join(map(str,dev.reg[int(ins[-3:],2)])),2)]=dev.reg[int(ins[-6:-3],2)]
        dev.pc+=1
    elif cmp(ins[0:10],"0011000000")==0:  
        ins_num=5
        ins_str="---\nMove reg["+str(int(ins[-3:],2))+"]"+" into reg["+str(int(ins[-6:-3],2))+"]\n"
        dev.reg[int(ins[-6:-3],2)] = dev.reg[int(ins[-3:],2)]
        dev.pc+=1
    elif cmp(ins[0:10],"0011000100")==0:  
        ins_num=6
        ins_str="---\nSwap reg["+str(int(ins[-6:-3],2))+"]"+", reg["+str(int(ins[-3:],2))+"]\n"
        dev.reg[int(ins[-6:-3],2)],dev.reg[int(ins[-3:],2)],dev.tmp[0] = dev.reg[int(ins[-3:],2)],dev.reg[int(ins[-6:-3],2)],dev.reg[int(ins[-6:-3],2)]
        dev.pc+=1
    elif cmp(ins[0:13],"0100000000000")==0:  
        ins_num=7
        ins_str="Push Rx\n"
    elif cmp(ins[0:10],"0101000000")==0:  
        ins_num=8
        ins_str="---\nAdd reg["+str(int(ins[-6:-3],2))+"]"+", reg["+str(int(ins[-3:],2))+"]\n"
        dev.carry=0
        tmp=[]
        dev.X=dev.reg[int(ins[-6:-3],2)]
        dev.Y=dev.reg[int(ins[-3:],2)]
        for i in range(15,-1,-1):
            tmp2=dev.reg[int(ins[-3:],2)][i]+dev.reg[int(ins[-6:-3],2)][i]+dev.carry
            if tmp2 >= 2:
                dev.carry=1
            else:
                dev.carry=0
            tmp.append(tmp2%2)
        dev.W=tmp[::-1]
        dev.reg[int(ins[-6:-3],2)]=tmp[::-1]
        dev.pc+=1
    elif cmp(ins[0:10],"0101000100")==0:  
        ins_num=9
        ins_str="Mul Rx, Ry\n"
        dev.pc+=1
    elif cmp(ins[0:13],"0100000100000")==0:  
        ins_num=10
        ins_str="Pop Rx\n"
    elif cmp(ins[0:10],"0101001000")==0:  
        ins_num=11
        ins_str="---\nSub reg["+str(int(ins[-6:-3],2))+"]"+", reg["+str(int(ins[-3:],2))+"]\n"
        dev.X=dev.reg[int(ins[-6:-3],2)]
        dev.Y=dev.reg[int(ins[-3:],2)]
        x=int("".join(map(str,dev.X)),2)
        y=int("".join(map(str,dev.Y)),2)
        if x-y<0:
            dev.carry=1;
            tmp=list(bin(y-x)[2:].zfill(16))
        else:
            dev.carry=0;
            tmp=list(bin(x-y)[2:].zfill(16))
        dev.W=map(int,tmp)
        dev.reg[int(ins[-6:-3],2)]=dev.W
        dev.pc+=1
    elif cmp(ins[0:10],"0101001100")==0:  
        ins_num=12
        ins_str="Div Rx, Ry\n"
        dev.pc+=1
    elif cmp(ins[0:10],"0101010000")==0:  
        ins_num=13
        ins_str="---\nAnd reg["+str(int(ins[-6:-3],2))+"]"+", reg["+str(int(ins[-3:],2))+"]\n"
        dev.X=dev.reg[int(ins[-6:-3],2)]
        dev.Y=dev.reg[int(ins[-3:],2)]
        x=int("".join(map(str,dev.X)),2)
        y=int("".join(map(str,dev.Y)),2)
        dev.W=list(bin(x&y)[2:].zfill(16))
        dev.reg[int(ins[-6:-3],2)]=dev.W
        dev.pc+=1
        ins_str+="output: "+bin(x^y)[2:].zfill(16)+"\n"
    elif cmp(ins[0:10],"0101010100")==0:  
        ins_num=14
        ins_str="---\nXor reg["+str(int(ins[-6:-3],2))+"]"+", reg["+str(int(ins[-3:],2))+"]\n"
        dev.X=dev.reg[int(ins[-6:-3],2)]
        dev.Y=dev.reg[int(ins[-3:],2)]
        x=int("".join(map(str,dev.X)),2)
        y=int("".join(map(str,dev.Y)),2)
        dev.W=list(bin(x^y)[2:].zfill(16))
        dev.reg[int(ins[-6:-3],2)]=dev.W
        dev.pc+=1
        ins_str+="output: "+bin(x^y)[2:].zfill(16)+"\n"
    elif cmp(ins[0:10],"0101011000")==0:  
        ins_num=15
        ins_str="---\nOr reg["+str(int(ins[-6:-3],2))+"]"+", reg["+str(int(ins[-3:],2))+"]\n"
        dev.X=dev.reg[int(ins[-6:-3],2)]
        dev.Y=dev.reg[int(ins[-3:],2)]
        x=int("".join(map(str,dev.X)),2)
        y=int("".join(map(str,dev.Y)),2)
        dev.W=list(bin(x|y)[2:].zfill(16))
        dev.reg[int(ins[-6:-3],2)]=dev.W
        ins_str+="output: "+bin(x|y)[2:].zfill(16)+"\n"
        dev.pc+=1
    elif cmp(ins[0:8],"01110000")==0:  
        ins_num=16
        ins_str="---\nFrom "+str(dev.pc)
        dev.pc=int(inputcode[dev.pc+1],2)
        ins_str+=" jump to "+str(dev.pc)+"\n"        
    elif cmp(ins[0:8],"01110001")==0:  
        ins_num=17
        ins_str="---\nFrom "+str(dev.pc)
        if dev.carry==1:
            dev.pc=int(inputcode[dev.pc+1],2)
            ins_str+=" jump to "+str(dev.pc)+"\n"
        else:
            ins_str+="fail to jump to "+str(int(inputcode[dev.pc+1],2))+" (because c is 0)\n"
            dev.pc+=2
        
    elif cmp(ins[0:8],"01110010")==0:  
        ins_num=18
        ins_str="---\nFrom "+str(dev.pc)
        if dev.carry==0:
            dev.pc=int(inputcode[dev.pc+1],2)
            ins_str+=" jump to "+str(dev.pc)+"\n"
        else:
            ins_str+="fail to jump to "+str(int(inputcode[dev.pc+1],2))+" (because c is 1)\n"
            dev.pc+=2
    elif cmp(ins[0:8],"01110011")==0:  
        ins_num=19
        dev.carry=0
        ins_str="---\nClear carry to 0\n"
        dev.pc+=1
    elif cmp(ins[0:8],"10110100")==0:  
        ins_num=20
        dev.carry=1
        ins_str="---\nSet carry to 1\n"
        dev.pc+=1
    elif cmp(ins[0:16],"1111111000000000")==0:  
        ins_num=22
        dev.pc+=2
        dev.mem[dev.sp]=list(bin(dev.pc)[2:].zfill(16))
        dev.mem_offset=dev.sp
        dev.pc=int(inputcode[dev.pc-2+1],2)
        ins_str="---\nCall function to "+inputcode[dev.pc-2+1]+" = "+str(dev.pc)+"\n"
        dev.sp+=1
        
    elif cmp(ins[0:16],"1111111100000000")==0:  
        ins_num=23
        dev.sp-=1
        dev.pc=int("".join(map(str,dev.mem[dev.sp])),2)
        dev.mem_offset=dev.pc   
        ins_str="---\nReturn to "+str(dev.pc)+"\n"
    else:
        ns_num=999
        dev.pc+=1
        ins_str="undefine yet\n"
    return ins_str
    

class Hardware:
    def __init__(self,mem_size=20):
        self.mem_size=mem_size
        self.mem=[[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] for _ in range(mem_size)]
        self.reg=[[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] for _ in range(8)]
        self.tmp=[[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0] for _ in range(2)]
        self.mar=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.X=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.Y=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.W=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.carry=0
        self.pc=-1
        self.sp=0xff00
    
    def show_mem(self,offset=0,how="b"):
        c=offset
        a=[]
        if how is "b":
            for i in self.mem[offset:offset+20]:
                a.append(("mem "+hex(c).zfill(6)+": "+("".join(map(str,i)))+"\n"))
                c += 1
        elif how is "h":
            for i in self.mem[offset:offset+20]:
                a.append(("mem "+hex(c).zfill(6)+": "+"%32s" % str(int("".join(map(str,i)),2).__hex__())+"\n"))
                c += 1
        elif how is "d":
            for i in self.mem[offset:offset+20]:
                a.append(("mem "+hex(c).zfill(6)+": "+"%31s" %str(int("".join(map(str,i)),2)).zfill(5)+"\n"))
                c += 1
        return "".join(a)
        
    def show_reg(self,how="b"):
        c=0
        a=[]
        if how is "b":
            for i in self.reg:
                a.append(("reg "+str(c)+": "+("".join(map(str,i)))+"\n"))
                c += 1
        elif how is "h":
            for i in self.reg:
                a.append(("reg "+str(c)+": "+"%32s" % str(int("".join(map(str,i)),2).__hex__().zfill(4))+"\n"))
                c += 1
        elif how is "d":
            for i in self.reg:
                a.append(("reg "+str(c)+": "+"%31s" %str(int("".join(map(str,i)),2)).zfill(5)+"\n"))
                c += 1
        return "".join(a)
        
    def show_alu(self,how="b"):
        a="ALU\n"
        if how is "b":
            a +="X: "+("".join(map(str,self.X)))+"\n"
            a +="Y: "+("".join(map(str,self.Y)))+"\n"
            a +="W: "+("".join(map(str,self.W)))+"\n"
            a +="C: "+str(self.carry)+"                                  \n"
        elif how is "d":
            a +="X: "+int(("".join(map(str,self.X))),2).__str__()+"\n"
            a +="Y: "+int(("".join(map(str,self.Y))),2).__str__()+"\n"
            a +="W: "+int(("".join(map(str,self.W))),2).__str__()+"\n"
            a +="C: "+str(self.carry)+"\n"
        elif how is "h":
            a +="X: "+int(("".join(map(str,self.X))),2).__hex__()+"\n"
            a +="Y: "+int(("".join(map(str,self.Y))),2).__hex__()+"\n"
            a +="W: "+int(("".join(map(str,self.W))),2).__hex__()+"\n"
            a +="C: "+str(self.carry)+"\n"          
        return a
        
    def show_tmp(self,how="b"):
        a="Temp\n"
        if how is "b":
            a+="Temp1: "+("".join(map(str,self.tmp[0])))+"\n"
            a+="Temp2: "+("".join(map(str,self.tmp[1])))+"\n"
        elif how is "d":
            a+="Temp1: "+int(("".join(map(str,self.tmp[0]))),2).__str__()+"\n"
            a+="Temp2: "+int(("".join(map(str,self.tmp[1]))),2).__str__()+"\n"
        elif how is "h":
            a+="Temp1: "+int(("".join(map(str,self.tmp[0]))),2).__hex__()+"\n"
            a+="Temp2: "+int(("".join(map(str,self.tmp[1]))),2).__hex__()+"\n" 
        return a
        
    def show_mar(self,how="b"):
        a="MAR\n"
        if how is "b":
            a+="MAR: "+("".join(map(str,self.mar)))+"\n"
        elif how is "h":
            a+="MAR: "+int(("".join(map(str,self.mar))),2).__hex__()+"\n"
        elif how is "d":
            a+="MAR: "+int(("".join(map(str,self.mar))),2).__str__()+"\n"
        return a

        
if __name__ == '__main__':
    a=Hardware()
    a=input()