import sys

def dtob(input, co):
	bi = bin(input)
	bi = bi[2 : len(bi)]
	zero = '0' * (co-len(bi))
	bi = zero + bi
	return bi
file_name = sys.argv[1]
reserved = ['Load', 'Store', 'Move', 'Swap', 'Add', 'Sub', 'Mul', 'Div', 'And', 'Or', 'Xor', 'Set.C', 'Clear.C', 'Jump.C', 'Jump.NC', 'Jump', 'Push', 'Pop']
out = []
label_map = {}
label_list = {}
variable_map = {}
variable_declare = []
file = open(file_name, 'r', encoding = 'UTF-8')
for line in file.readlines():
	list = line.split()
	if list[0] not in reserved :
		if list[1] not in reserved:
			variable_map[list[0]] = len(variable_declare) + 2
			if '0b' in list[1] :
				variable_declare.append(dtob(int(list[1], 2), 16))
			elif '0x' in list[1] :
				variable_declare.append(dtob(int(list[1], 16), 16))
			elif '[' in list[1] :
				length = list[1].split('[')
				length = length[1].split(']')
				length = int(length[0])
				for i in range(length) :
					variable_declare.append('0000000000000000')
			else:
				variable_declare.append(dtob(int(list[1]), 16))
			continue
		else:
			label_map[list[0]] = len(out)
		list = list[1:len(list)]
		
	#print(list)
	if list[0] == 'Load' :
		Rx = int(list[1][1])		
		if 'R' in list[2] :
			tmp = list[2].split('R')
			Ry= int(tmp[1][0])
			machine_code = '0001000100' + dtob(Rx, 3) + dtob(Ry, 3)
			out.append(machine_code)
		elif 'mem' in list[2]:
			#print(list[0] + ' ' + list[1])
			list = list[2].split('[')
			list = list[1].split(']')
			machine_code = '0001001000000' + dtob(Rx, 3)
			out.append(machine_code)
			if '0x' in list[0]:
				addr = dtob(int(list[0],16), 16)
				out.append(addr)
			else:
				if list[0] in label_list:
					#print('THERE = ' + list[0])
					label_list[list[0]].append(len(out))
					out.append('EMPTY')
				elif list[0] in variable_map:
					#print('HERE = ' + list[0])
					out.append(dtob(variable_map[list[0]], 16))
				else:
					#print('ERE = ' + list[0])
					label_list[list[0]] =[len(out)]
					out.append('EMPTY')
		elif '#' in list[2]:
			machine_code = '0001000000000' + dtob(Rx, 3)
			out.append(machine_code)
			list = list[2].split('#')
			if '0x' in list[1]:
				num = dtob(int(list[1],16), 16)
				out.append(num)
			elif '0b' in list[1]:
				num = dtob(int(list[1],2), 16)
				out.append(num)
			else:
				num = dtob(int(list[1]), 16)
				out.append(num)
	elif list[0] == 'Store' :
		Rx = int(list[2][1])
		list = list[1].split('[')
		list = list[1].split(']')
		out.append('0010000000000' + dtob(Rx,3))
		if '0x' in list[0]:
			addr = dtob(int(list[0],16), 16)
			out.append(addr)
		else:
			if list[0] in label_list:
				label_list[list[0]].append(len(out))
				out.append('EMPTY')
			elif list[0] in variable_map :
				out.append(dtob(variable_map[list[0]], 16))
			else:
				label_list[list[0]] =[len(out)]
				out.append('EMPTY')
			#addr = dtob(int(list[0],16), 16)
			#out.append(addr)
	elif list[0] == 'Move' or list[0] == 'Swap' or list[0] == 'Add' or list[0] == 'Sub' or list[0] == 'Mul' or list[0] == 'Div' or list[0] == 'And' or list[0] == 'Or' or list[0] == 'Xor':
		Rx = list[1].split('R')
		Rx = Rx[1].split(',')
		Rx = int(Rx[0])
		Ry = list[2].split('R')
		Ry = int(Ry[1])
		if list[0] == 'Move':
			out.append('0011000000' + dtob(Rx,3) + dtob(Ry, 3))
		elif list[0] == 'Swap':
			out.append('0011000100' + dtob(Rx,3) + dtob(Ry, 3))
		elif list[0] == 'Add':
			out.append('0101000000' + dtob(Rx,3) + dtob(Ry, 3))
		elif list[0] == 'Sub':
			out.append('0101001000' + dtob(Rx,3) + dtob(Ry, 3))
		elif list[0] == 'Mul':
			out.append('0101000100' + dtob(Rx,3) + dtob(Ry, 3))
		elif list[0] == 'Div':
			out.append('0101001100' + dtob(Rx,3) + dtob(Ry, 3))
		elif list[0] == 'And':
			out.append('0101010000' + dtob(Rx,3) + dtob(Ry, 3))
		elif list[0] == 'Or':
			out.append('0101011000' + dtob(Rx,3) + dtob(Ry, 3))
		elif list[0] == 'Xor' :
			out.append('0101010100' + dtob(Rx,3) + dtob(Ry, 3))
	elif list[0] == 'Push':
		out.append('0100000000000' + dtob(int(list[1][1]),3))
	elif list[0] == 'Pop':
		out.append('0100000100000' + dtob(int(list[1][1]),3))
	elif 'Jump' in list[0] :
		if '.NC' in list[0] :
			out.append('0111001000000000')
		elif '.C' in list[0] :
			out.append('0111000100000000')
		elif list[0] == 'Jump' :
			out.append('0111000000000000')
		if '0x' in list[1] :
			addr = dtob(int(list[1], 16), 16)
			out.append(addr)
		else :
			if list[1] in label_list :
				label_list[list[1]].append(len(out))
				out.append("EMPTY")
			elif list[1] in variable_map:
				out.append(dtob(variable_map[list[1]], 16))
			else :
				label_list[list[1]] = [len(out)]
				out.append("EMPTY")
	elif 'Set' in list[0] :
		#print("LALALA")
		out.append('1011010000000000')
	elif 'Clear' in list[0] :
		out.append('0111001100000000')
for label in label_list :
	#print('label = ' + label)
	for i in label_list[label]:
		out[i] = dtob(label_map[label] + len(variable_declare) + 2, 16)
		#out[i] = label_map[label] + len(variable_declare) + 1
print('0111000000000000')
print(dtob(len(variable_declare) + 2, 16))
for i in variable_declare:
	print(i)
for i in out:
	print(i)
file.close()