module Computer0m(clk_50mhz);
wire [15:0] ir; //IR
wire [15:0] mar; //MAR
wire [15:0] mdr; //MDR
wire [15:0] dbus; //dataBus
wire [15:0] aluOut; //W

cpu0m cpu (clk_50mhz, .pc(pc), .ir(ir), .mar(mar), .mdr(mdr), .dbus(dbus), .m_en(m_en), .m_rw(m_rw), .aluOut(aluOut));

endmodule

module cpu0m(clk_50mhz,  output reg [15:0] ir, output reg [15:0] mar,  output reg [15:0] mdr, inout [15:0] dbus,  output reg m_en, m_rw, output reg signed [15:0] aluOut);
	input key;//input number
	reg reset;
	reg print;
	reg printans;
	reg ifpoint;
	reg notzero;
	reg [5:0] which,need,whichans,need1,need2;
	reg [28:0]counter,temptime;
	reg [7:0] memptr;
	reg [7:0] op0;
	reg [7:0] op1;
	reg [15:0] op2;
    reg signed [15:0] X;
    reg signed [15:0] Y;
	reg signed  C;
    reg signed [15:0] TEMP1;
	reg signed [15:0] TEMP2;
    reg signed [16:0] R [0:8]; //Register
	reg signed [16:0] ALU;
    reg [2:0] ra;
    reg [2:0] rb;
	reg [4:0] gar1;
	reg [1:0] gar2;
	reg [3:0] point;
	input [0:4] swnum;
	input [11:0] swcul;
	reg [0:15] prem [0:11]
	reg [0:15] m [0:4095];
	/*memory :  data in (the number on sw) is always at m[14~27] 
					   answer is always at m[0~13]
	*/
	output [7:0]oLCD_data;
	output olcd_rw,olcd_en,olcd_rs,olcd_on;
	reg [7:0]LCD_data; 
	reg [15:0] data;
	
	assign oLCD_data=LCD_data;
	assign olcd_rw=lcd_rw;
	assign olcd_en=lcd_en;
	assign olcd_rs=lcd_rs;
	assign olcd_on=lcd_on;
	
	initial
	begin
	prem[0][0:7] = 8'b00100000;
	prem[1][0:7] = 8'b00100000;
	prem[2][0:7] = 8'b00100000;
	prem[3][0:7] = 8'b00100000;
	prem[4][0:7] = 8'b00100000;
	prem[5][0:7] = 8'b00100000;
	prem[6][0:7] = 8'b00100000;
	prem[7][0:7] = 8'b00100000;
	prem[8][0:7] = 8'b00100000;
	prem[9][0:7] = 8'b00100000;
	prem[10][0:7] = 8'b00100000;
	prem[11][0:7] = 8'b00100000;
	point = 0;//小數點後有幾位
	ifpoint = 0;//是否有小數點
	reset = 1;
	notzero = 0;
	key = 1;
	R[3] = 0;
	print = 0;
	memptr = 0;
	printans = 0;
	counter = 0;
	which = 6'd0;
	need = 6'd13;
	need1 = 6'd3;
	need2 = 6'd13;
	end
	
    `define PC R[8] 
	
   always @(posedge clk_50mhz or posedge swcul) begin
		if(key = 0) print = 1;
		else;
		if( print == 1)begin//print out input number from LCD
			if(which > need)  //done 進入等待
			begin
				which = 6'd0;
				counter = 28'd0;
				print = 1'b0;
			end
			else if(which <= need) //還沒寫入need個指令
			begin
                if(which == 6'd0)begin
                    tmptime=counter; //暫時存起時間
					prem[0:10] = prem[1:11];
					if(swnum < 16)begin//not 小數點
						prem[11][0:3] = swnum[0:3]
						if(ifpoint == 1)point = point +1;
						else;
					end
					else begin
						prem[11] = 10;//小數點 = 10
						ifpoint = 1;
						if(prem[10][0:7] == 8'b00100000)begin//如果小數點前沒有數字
							prem[10][0:7] = 8'b00000000;//前一個數字變0
							memptr = memptr + 1;
						end
						else;
					end
					memptr = memptr + 1;
				end
                else;
                if(which < 2)    //前兩個指令  必為clear 跟00001111
                begin
                lcd_rs = 1'b0;   //rs 設為command
                case(which)
                        6'd0:   LCD_data = 8'b00000001;
                        6'd1:   LCD_data=8'b00001111;
                        default : LCD_data = 8'b00001111;
                endcase
                end         
                else         //which>2  視sw
                begin
                        lcd_rs=1'b1;
                        case(which)
                                6'd2:begin
                                        case(prem[0])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
									 end
                                6'd3:begin
                                        case(prem[1])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
									 end
                                6'd4:begin
                                        case(prem[2])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
									 end
                                6'd5:begin
                                        case(prem[3])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
									 end
                                6'd6:begin
                                        case(prem[4])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
									 end
                                6'd7:begin
                                        case(prem[5])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
									 end
								6'd8:begin
                                        case(prem[6])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
									 end
								6'd9:begin
                                        case(prem[7])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
									 end
								6'd10:begin
                                        case(prem[8])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
									 end
								6'd11:begin
                                        case(prem[9])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
									 end
								6'd12:begin
                                        case(prem[10])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
									 end
								6'd13:begin
                                        case(prem[11])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
									 end
                        endcase
                end
                if( counter >= tmptime + 28'd80000 ) //  等到40000以上 拉低enable,counter 歸零,which+1 
                begin 
                lcd_en=1'b0;   
                counter=28'd0;
                if(which<=need)
                        which = which + 6'd1;
                end
                else if(counter>=tmptime+28'd40000)// 等到20000以上  拉高enable
                begin
                        lcd_en=1'b1;
                        counter = counter + 28'd1;
                end
                else
                        counter = counter + 28'd1;
			end
        end
		else;
		if(swcul != 0)begin//map prem to m
			m[(26 - memptr):25] = prem[(12 - memptr):11];//memory 14~25 放數字
			m[26][0:3] = point; //26 放小數點
			memptr = 0;
		end
		else;
		case(swcul)// judge the operator
			0:    R[0] = 0;
			1:    R[0] = ;
			2:    R[0] = ;
			4:    R[0] = ;
			8:    R[0] = ;
			16:   R[0] = ;
			32:   R[0] = ;
			64:   R[0] = ;
			128:  R[0] = ;
			256:  R[0] = ;
			512:  R[0] = ;
			1024: R[0] = ;
			2048: R[0] = ;	
		endcase
        if (reset) begin//reset print out 0
            `PC = 0;
			reset = 0;
			if(which > need1)  //done 進入等待
			begin
				which = 6'd0;
				counter = 28'd0;
			end
			else if(which <= need1) //還沒寫入need個指令
			begin
                if(which == 6'd0)
                    tmptime=counter; //暫時存起時間
                else;
                if(which < 2)    //前兩個指令  必為clear 跟00001111
                begin
                lcd_rs = 1'b0;   //rs 設為command
                case(which)
                        6'd0:   LCD_data = 8'b00000001;
                        6'd1:   LCD_data=8'b00001111;
                        default : LCD_data = 8'b00001111;
                endcase
                end         
                else         //which>2  視sw
                begin
                        lcd_rs=1'b1;
                        case(which)
                                6'd2: LCD_data = 8'b00110000;//0
                                default: ;
                        endcase
                end
                if( counter >= tmptime + 28'd80000 ) //  等到40000以上 拉低enable,counter 歸零,which+1 
                begin 
                lcd_en=1'b0;   
                counter=28'd0;
                if(which<=need1)
                        which = which + 6'd1;
                end
                else if(counter>=tmptime+28'd40000)// 等到20000以上  拉高enable
                begin
                        lcd_en=1'b1;
                        counter = counter + 28'd1;
                end
                else
                        counter = counter + 28'd1;
			end
        end
        else if( print == 0 && printans == 0)begin//get & do machine code
                mar = `PC; // MAR = PC
				//FLASHR;
                `PC = `PC+1; // PC = PC + 1
                {op0, op1} = dbus;
                case (op0) // 解讀 OP(第一部分>前8bits)
					8'b00000000: begin//endcul
							R[3] = 0;
							reset = 1;
							memptr = 14;
							printans = 1;
						end
                     8'b00010000: begin //  Load Rx, #imm 
                            {gar1, ra} = op1; // get Rx
							mar = `PC; // MAR = PC
							//FLASHR;
							`PC = `PC+1; // PC = PC + 1
                            R[ra] = dbus; // get imm data, set to Rx
                        end
                    8'b00010001: begin // Load Rx, mem[Ry]
                            {gar2, ra, rb} = op1; // get Rx Ry
							dbus = R[rb];
							mar = dbus;
							dbus = m[mar];
                            R[ra] = dbus; // set to Rx
                        end
                    8'b00010010: begin // Load Rx, mem[addr]
                            {gar1, ra} = op1; // get Rx
							mar = `PC; // MAR = PC
							//FLASHR;
							`PC = `PC+1; // PC = PC + 1
							mar  = dbus; // get addr -> MAR
							dbus = m[mar];
                            R[ra] = dbus; // get mem[addr], set to Rx
                        end
					8'b00100000: begin // Store mem[addr], Rx
							{gar1, ra} = op1; // get Rx
							mar = `PC; // MAR = PC
							//FLASHR;
							`PC = `PC+1; // PC = PC + 1
							mar  = dbus; // get addr -> MAR
							dbus = R[ra];
							m[mar] = dbus;
                        end
					8'b00110000: begin // Move Rx, Ry  
							{gar2, ra, rb} = op1; // get Rx Ry
							dbus = R[ra];// set to Ry
							R[rb] =  dbus;
                        end					
					8'b00110001: begin // Swap Rx, Ry 
							{gar2, ra, rb} = op1; // get Rx Ry
							dbus = R[ra];
							TEMP1 = dbus;
							dbus = R[rb];// set to Rx
							R[ra] = dbus;
							dbus = TEMP1;
							R[rb] = dbus;
						end
					8'b01000000: begin // Push Rx 
							{gar1, ra} = op1; // get Rx
							dbus = R[ra];
							TEMP2 = dbus;
						end
					8'b01010000: begin // Add Rx, Ry  
							{gar2, ra, rb} = op1; // get Rx Ry
							dbus = R[ra];
							X = dbus;
							dbus = R[rb];
							Y = dbus;
							ALU = X + Y;
							{C, aluOut} = ALU;
							dbus = aluOut;
							R[ra] = dbus;
						end
					8'b01000001: begin // Pop Rx 
						{gar1, ra} = op1; // get Rx
							dbus = TEMP2;
							R[ra] = dbus;
						end
					8'b01010010: begin // Sub Rx, Ry 
							{gar2, ra, rb} = op1; // get Rx Ry
							dbus = R[ra];
							X = dbus;
							dbus = R[rb];
							Y = dbus;
							aluOut = X - Y;
							dbus = aluOut;
							R[ra] = dbus;
						end
					8'b01010100: begin // And Rx, Ry
							{gar2, ra, rb} = op1; // get Rx Ry
							dbus = R[ra];
							X = dbus;
							dbus = R[rb];
							Y = dbus;
							aluOut = X & Y;
							dbus = aluOut;
							R[ra] = dbus;
						end					
					8'b01010101: begin // Xor Rx, Ry 
							{gar2, ra, rb} = op1; // get Rx Ry
							dbus = R[ra];
							X = dbus;
							dbus = R[rb];
							Y = dbus;
							aluOut = X ^ Y;
							dbus = aluOut;
							R[ra] = dbus;
						end
					8'b01010110: begin // Or Rx, Ry  
							{gar2, ra, rb} = op1; // get Rx Ry
							dbus = R[ra];
							X = dbus;
							dbus = R[rb];
							Y = dbus;
							aluOut = X | Y;
							dbus = aluOut;
							R[ra] = dbus;
						end					
					8'b01110000: begin // Jump Loc(a label in asm)        
							mar = `PC; // MAR = PC
							m_rw = 1; // m_rw=1 is read mode, read(m[MAR]) => read(m[PC])
							//FLASHR;
							 `PC = dbus; // get addr -> MAR
						end
					8'b01110001: begin // Jump.C Loc(a label in asm) 
							dbus = `PC;
							X = dbus ;
							ALU = X+!C;
							{C, aluOut} = ALU;
							dbus = aluOut;
							`PC = dbus;
						end
					8'b01110010: begin // Jump.NC Loc(a label in asm)  
							dbus = `PC;
							X = dbus ;
							ALU = X+C;
							{C, aluOut} = ALU;
							dbus = aluOut;
							`PC = dbus;
						end
					8'b01110011: begin // Clear.C      
							C = 0;
						end
					8'b10110100: begin // Set.C    
							C = 1;
						end
                    default:    begin
                        end
                    endcase
        end
		else;
		if(printans = 1)begin//print ans to LCD 
			if(which > need2)  //done 進入等待
			begin
				which = 6'd0;
				counter = 28'd0;
				printans = 1'b0;
			end
			else if(which <= need) //還沒寫入need個指令
			begin
                if(which == 6'd0)begin
                    tmptime=counter; //暫時存起時間
				end
                else;
                if(which < 2)    //前兩個指令  必為clear 跟00001111
                begin
                lcd_rs = 1'b0;   //rs 設為command
                case(which)
                        6'd0:   LCD_data = 8'b00000001;
                        6'd1:   LCD_data=8'b00001111;
                        default : LCD_data = 8'b00001111;
                endcase
                end         
                else         //which>2  視sw
                begin
                        lcd_rs=1'b1;
                        case(which)
                                6'd2:begin
										if(m[0] == 0 && notsero == 0) LCD_data = 8'b00100000;//
										else begin
										notzero = 1;
                                        case(m[0])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
										end
									 end
                                6'd3:begin
                                        if(m[1] == 0 && notsero == 0) LCD_data = 8'b00100000;//
										else begin
										notzero = 1;
                                        case(m[1])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
										end
									 end
                                6'd4:begin
                                        if(m[2] == 0 && notsero == 0) LCD_data = 8'b00100000;//
										else begin
										notzero = 1;
                                        case(m[2])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
										end
									 end
                                6'd5:begin
                                        if(m[3] == 0 && notsero == 0) LCD_data = 8'b00100000;//
										else begin
										notzero = 1;
                                        case(m[3])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
										end
									 end
                                6'd6:begin
                                        if(m[4] == 0 && notsero == 0) LCD_data = 8'b00100000;//
										else begin
										notzero = 1;
                                        case(m[4])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
										end
									 end
                                6'd7:begin
                                        if(m[5] == 0 && notsero == 0) LCD_data = 8'b00100000;//
										else begin
										notzero = 1;
                                        case(m[5])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
										end
									 end
								6'd8:begin
                                        if(m[6] == 0 && notsero == 0) LCD_data = 8'b00100000;//
										else begin
										notzero = 1;
                                        case(m[6])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
										end
									 end
								6'd9:begin
                                        if(m[7] == 0 && notsero == 0) LCD_data = 8'b00100000;//
										else begin
										notzero = 1;
                                        case(m[7])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
										end
									 end
								6'd10:begin
                                        if(m[8] == 0 && notsero == 0) LCD_data = 8'b00100000;//
										else begin
										notzero = 1;
                                        case(m[8])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
										end
									 end
								6'd11:begin
                                        if(m[9] == 0 && notsero == 0) LCD_data = 8'b00100000;//
										else begin
										notzero = 1;
                                        case(m[9])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
										end
									 end
								6'd12:begin
                                        if(m[10] == 0 && notsero == 0) LCD_data = 8'b00100000;//
										else begin
										notzero = 1;
                                        case(m[10])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
										end
									 end
								6'd13:begin
                                        if(m[11] == 0 && notsero == 0) LCD_data = 8'b00100000;//
										else begin
										notzero = 1;
                                        case(m[11])
											   0:      LCD_data = 8'b00110000;//0
                                               1:      LCD_data = 8'b00110001;//1
                                               2:      LCD_data = 8'b00110010;//2
                                               3:      LCD_data = 8'b00110011;//3
                                               4:      LCD_data = 8'b00110100;//4
                                               5:      LCD_data = 8'b00110101;//5
                                               6:      LCD_data = 8'b00110110;//6
                                               7:      LCD_data = 8'b00110111;//7
                                               8:      LCD_data = 8'b00111000;//8
											   9:      LCD_data = 8'b00111001;//9
											   10:      LCD_data = 8'b00101110;//.
                                               default:        LCD_data = 8'b00100000;//
                                        endcase
										end
									 end
                        endcase
                end
                if( counter >= tmptime + 28'd80000 ) //  等到40000以上 拉低enable,counter 歸零,which+1 
                begin 
                lcd_en=1'b0;   
                counter=28'd0;
                if(which<=need)
                        which = which + 6'd1;
                end
                else if(counter>=tmptime+28'd40000)// 等到20000以上  拉高enable
                begin
                        lcd_en=1'b1;
                        counter = counter + 28'd1;
                end
                else
                        counter = counter + 28'd1;
			end
		end
		else;
    end
	
	task FLASHR;
	begin
		//let flashaddr = `PC
		//start read from Flash
		//store in dbus
	end
	
endmodule
